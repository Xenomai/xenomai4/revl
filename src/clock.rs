//! Interface to EVL clocks.
//!
//! The target platform can provide particular clock chips and/or
//! clock source drivers in addition to the architecture-specific
//! ones. For instance, some device on a PCI bus could provide a timer
//! which the application wants to use for timing its threads, in
//! addition to the architected timer found on ARM64 and some
//! ARM-based SoCs. The timer hardware would be accessed through the
//! EVL core via a dedicated kernel driver. EVL's clock element
//! ensures all clock drivers present the same interface to
//! applications in user-space.
//!
//! The Clock struct is the Rust interface to EVL's [clock
//! element](https://evlproject.org/core/user-api/clock/).

use std::io::{
    Error,
    ErrorKind
};
use libc::{
    self,
    c_int,
    c_long,
    time_t,
};
use evl_sys::{
    evl_read_clock,
    evl_sleep_until,
    timespec,
    BuiltinClock
};
use embedded_time::{
    clock,
    duration::{Nanoseconds, Seconds},
    rate::*,
    Clock, Instant,
};

/// A core system clock which has nanosecond resolution. This type is
/// a wrapper to an EVL [clock
/// element](https://evlproject.org/core/user-api/clock/).
///
/// The CoreClock struct is a facade to access built in clocks which
/// never vanish, therefore there is no attempt to discard/close the
/// latter upon Drop.
#[derive(PartialEq, Debug)]
pub struct CoreClock(pub(crate) BuiltinClock);

impl Clock for CoreClock {
    type T = u64;
    const SCALING_FACTOR: Fraction = Fraction::new(1, 1_000_000_000); // ns

    /// Get the current time.
    ///
    /// The timestamp is returned as an [Instant
    /// struct](https://docs.rs/embedded-time/latest/embedded_time/struct.Instant.html)
    /// representing the count of elapsed nanoseconds since the epoch
    /// for this clock.
    ///
    /// # Errors
    ///
    /// The current implementation never fails.
    fn try_now(&self) -> Result<Instant<Self>, clock::Error> {
        let mut now = timespec {
            tv_sec: 0,
            tv_nsec: 0,
        };
        unsafe { evl_read_clock(self.0 as c_int, &mut now) };
        let now_ns: u64 = now.tv_sec as u64 * 1_000_000_000 + now.tv_nsec as u64;
        Ok(Instant::new(now_ns))
    }
}

impl CoreClock {
    /// Sleep until the absolute `timeout` date has been reached as
    /// measured by this clock.
    ///
    /// This call blocks the caller until the wake up date expires.
    ///
    /// # Errors
    ///
    /// [`ErrorKind::Interrupted`] if the call was interrupted by an
    /// in-band signal, or forcibly unblocked by the EVL core.
    ///
    /// [`ErrorKind::PermissionDenied`] if the calling thread is not
    /// attached to the EVL core.
    ///
    /// [`ErrorKind::Other`] on unexpected error.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use embedded_time::{
    ///     duration::*,
    ///     Clock,
    ///     Instant,
    /// };
    /// use revl::clock::*;
    ///
    /// let timeout = STEADY_CLOCK.now() + Seconds(1_u32);
    /// assert!(STEADY_CLOCK.sleep_until(timeout).is_ok())
    /// ```
    pub fn sleep_until(&self, timeout: Instant<CoreClock>) -> Result<(), Error> {
        let dur = timeout.duration_since_epoch();
        let secs: Seconds<u64> = Seconds::try_from(dur).map_err(
            |_| Error::from(ErrorKind::InvalidInput)
        )?;
        let nsecs: Nanoseconds<u64> = Nanoseconds::<u64>::try_from(dur).map_err(
            |_| Error::from(ErrorKind::InvalidInput)
        )?;
        let date = timespec {
            tv_sec: secs.integer() as time_t,
            tv_nsec: nsecs.integer() as c_long,
        };
        let ret: c_int = unsafe { evl_sleep_until(self.0 as c_int, &date) };
        match -ret {
            0 => {
                Ok(())
            },
            libc::EINTR => {
                Err(Error::from(ErrorKind::Interrupted))
            },
            libc::EPERM => {
                Err(Error::from(ErrorKind::PermissionDenied))
            },
            _ => {
                Err(Error::from(ErrorKind::Other))
            },
        }
    }

    /// Get the current time measured by this clock.
    ///
    /// The timestamp is returned as an [Instant
    /// struct](https://docs.rs/embedded-time/latest/embedded_time/struct.Instant.html)
    /// representing the count of elapsed nanoseconds since the epoch.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::clock::*;
    ///
    /// let now = STEADY_CLOCK.now();
    /// ```
    pub fn now(&self) -> Instant<Self> {
        self.try_now().unwrap()
    }
}

/// STEADY_CLOCK is a monotonic clock with nanosecond resolution. This
/// clock has the same semantics than its POSIX `CLOCK_MONOTONIC`
/// counterpart. This is the Rust interface to the built-in
/// [EVL_CLOCK_MONOTONIC](https://evlproject.org/core/user-api/clock/#builtin-clocks).
pub const STEADY_CLOCK: CoreClock = CoreClock(BuiltinClock::MONOTONIC);
/// SYSTEM_CLOCK is real-time wallclock with nanosecond resolution,
/// which may be subject to discontinuous jumps in time due to
/// adjustments. This clock has the same semantics than its POSIX
/// `CLOCK_REALTIME` counterpart. This is the Rust interface to the
/// built-in
/// [EVL_CLOCK_REALTIME](https://evlproject.org/core/user-api/clock/#builtin-clocks).
pub const SYSTEM_CLOCK: CoreClock = CoreClock(BuiltinClock::REALTIME);
