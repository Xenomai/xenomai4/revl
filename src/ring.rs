//! Lockless, MPMC, bounded FIFO queue.
//!
//! A ring queue is a MPMC data structure composed of two lockless
//! FIFO ring buffers and a data vector. The first FIFO stores indices
//! of messages pending receive which are available at the
//! corresponding cells from the data vector, the second FIFO stores
//! indices of free cells into the data vector. The implementation
//! performs no dynamic memory allocation.
//!
//! In practice, an index is pulled from the free cells queue, the
//! message is written to the data vector at this index, which is
//! eventually pushed to the pending cells queue. The receiver pulls
//! the next available index from the pending cells queue, extracts
//! the message at the corresponding position in the vector then
//! releases the consumed index to the free cells queue.
//!
//! The implementation does not require any EVL core services.
//!
//! The lighweight ring buffers are based on Ruslan Nikolaev's
//! Scalable Circular Queue ([single-width CAS
//! variant](https://github.com/rusnikola/lfqueue.git)) ported to
//! Rust. See
//! <http://drops.dagstuhl.de/opus/volltexte/2019/11335/pdf/LIPIcs-DISC-2019-28.pdf>.

use std::sync::{
    Arc,
    atomic::fence,
    atomic::AtomicUsize,
    atomic::AtomicIsize,
    atomic::Ordering::Acquire,
    atomic::Ordering::AcqRel,
    atomic::Ordering::Relaxed,
    atomic::Ordering::Release,
};
use std::mem;
use std::default::Default;
use core::cell::UnsafeCell;

// Conservative: 128 bytes should fit anything we run on. Bottom line:
// we want to prevent cacheline bouncing in SMP on hot data.
const CACHELINE_SHIFT: usize = 7;

#[cfg(target_pointer_width = "64")]
const RING_MIN_ORDER: usize = CACHELINE_SHIFT - 3;
#[cfg(target_pointer_width = "32")]
const RING_MIN_ORDER: usize = CACHELINE_SHIFT - 2;
const RING_EMPTY_VAL: usize = !0;

// Revisit: Rust align() attribute currently requires a literal,
// struct fields do not support alignment directives, complex const
// generics are not available from the stable channel yet, all of this
// is a bit of a pain at the moment. We just work around those
// limitations for now.

/// Cursor pointing at the head of a ring buffer, aligned on a
/// (conservative) cacheline size to prevent too much bouncing.
#[derive(Debug)]
#[repr(align(128))]             // CACHELINE_ALIGNMENT
struct Head {
    d: AtomicUsize,
}

/// Value of the threshold bound for livelock prevention.
#[derive(Debug)]
#[repr(align(128))]
struct Threshold {
    d: AtomicIsize,
}

/// Cursor pointing at the tail of a ring buffer, aligned on the
/// cacheline size to prevent too much bouncing.
#[derive(Debug)]
#[repr(align(128))]
struct Tail {
    d: AtomicUsize,
}

fn sub_with_overflow(lhs: usize, rhs: usize) -> isize {
    lhs.overflowing_sub(rhs).0 as isize
}

/// The FIFO ring buffer. Each ring queue is composed of two of those
/// buffers, one storing indices of messages pending receive which are
/// available at the corresponding cells from the data vector, another
/// storing indices of free cells into the data vector.
#[derive(Debug)]
#[repr(align(128))]
// Revisit when we have complex const generics, so that we can
// define an array inline [ AtomicUsize; 1 << (ORDER + 1) ].
struct Ring<const ORDER: usize> {
    /// Vector of indices.
    cells: Vec<AtomicUsize>,
    /// Cursor to the current head of the ring.
    head: Head,
    /// Value of the threshold bound.
    threshold: Threshold,
    /// Cursor to the current tail of the ring.
    tail: Tail,
}

impl<const ORDER: usize> Ring<ORDER> {
    /// Create a new FIFO ring buffer with 1 << `ORDER` entries
    /// available.
    fn new() -> Self {
        let nr_cells = Ring::<ORDER>::get_nr_cells();
        let mut this = Self {
            // To maintain every single ring entry, we need two cells.
            cells: Vec::with_capacity(nr_cells),
            head: Head { d: AtomicUsize::new(0) },
            tail: Tail { d: AtomicUsize::new(0) },
            threshold: Threshold { d: AtomicIsize::new(-1) },
        };
        // Populate the vector.
        this.cells.resize_with(nr_cells, || { AtomicUsize::new(RING_EMPTY_VAL) });
        this
    }

    /// Fill the ring buffer.
    ///
    /// Push a sequence of indices from 1 to (1 << ORDER) - 1 to the
    /// ring buffer, so that it appears entirely filled.
    fn fill(&mut self) {
        let half: usize = Ring::<ORDER>::get_nr_entries();
        let full: usize = Ring::<ORDER>::get_nr_cells();
        for n in 0..half {
            self.cells[Ring::<ORDER>::map(n, full, ORDER + 1)].store(
                Ring::<ORDER>::map(full + n, half, ORDER), Relaxed
            );
        }
        for n in half..full {
            self.cells[Ring::<ORDER>::map(n, full, ORDER + 1)].store(
                RING_EMPTY_VAL, Relaxed
            );
        }
        self.head.d.store(0, Relaxed);
        self.tail.d.store(half, Relaxed);
        self.threshold.d.store(Ring::<ORDER>::get_threshold(half, full), Relaxed);
    }

    /// Push an index to the ring buffer.
    ///
    /// Enqueues `eidx` into the FIFO. This index must be in the range
    /// 0 .. (1 << ORDER) - 1.
    ///
    /// # Safety
    ///
    /// No check is performed on the validity of `eidx`. Any out of
    /// range value pushed to a ring buffer would certainly end up in
    /// a train wreck with extreme prejudice.
    fn enqueue(&self, mut eidx: usize) {
        let half: usize = Ring::<ORDER>::get_nr_entries();
        let full: usize = Ring::<ORDER>::get_nr_cells();
        eidx ^= full - 1;
        'again: loop {
            let tail = self.tail.d.fetch_add(1, AcqRel);
            let tcycle = (tail << 1) | (2 * full - 1);
            let tidx = Ring::<ORDER>::map(tail, full, ORDER + 1);
            let mut entry = self.cells[tidx].load(Acquire);
            loop {
                let ecycle = entry | (2 * full - 1);
                if sub_with_overflow(ecycle, tcycle) < 0 &&
                    (entry == ecycle ||
                     (entry == (ecycle ^ full) &&
                      sub_with_overflow(self.head.d.load(Acquire), tail) <= 0)) {
                        match self.cells[tidx].compare_exchange_weak(entry, tcycle ^ eidx, AcqRel, Acquire) {
                            Ok(_) => {
                                let t = Ring::<ORDER>::get_threshold(half, full);
                                if self.threshold.d.load(Relaxed) != t {
                                    self.threshold.d.store(t, Relaxed);
                                }
                                return;
                            },
                            Err(ret) => entry = ret,
                        }
                    } else {
                        continue 'again;
                    }
            }
        }
    }

    /// Pop an index from the ring buffer.
    ///
    /// Dequeues and returns the first available index from the FIFO,
    /// or None if empty.
    fn dequeue(&self) -> Option<usize> {
        if self.threshold.d.load(Relaxed) < 0 {
            return None;
        }
        let full: usize = Ring::<ORDER>::get_nr_cells();
        loop {
            let head = self.head.d.fetch_add(1, AcqRel);
            let hcycle = (head << 1) | (2 * full - 1);
            let hidx = Ring::<ORDER>::map(head, full, ORDER + 1);
            let mut attempt = 0;
            'again: loop {
                let mut entry = self.cells[hidx].load(Acquire);
                loop {
                    let ecycle = entry | (2 * full - 1);
                    if ecycle == hcycle {
                        self.cells[hidx].fetch_or(full - 1, AcqRel);
                        return Some(entry & (full - 1));
                    }
                    let new_entry;
                    if (entry | full) != ecycle {
                        new_entry = entry & (!full);
                        if entry == new_entry {
                            break;
                        }
                    } else {
                        attempt += 1;
                        if attempt <= 3000 {
                            continue 'again;
                        }
                        new_entry = hcycle ^ ((!entry) & full);
                    }
                    if sub_with_overflow(ecycle, hcycle) >= 0 {
                        break;
                    }
                    match self.cells[hidx].compare_exchange_weak(entry, new_entry, AcqRel, Acquire) {
                        Ok(_) => break,
                        Err(ret) => entry = ret,
                    }
                }
                let tail = self.tail.d.load(Acquire);
                if sub_with_overflow(tail, head + 1) <= 0 {
                    self.catchup(tail, head + 1);
                    self.threshold.d.fetch_sub(1, AcqRel);
                    return None;
                }
                if self.threshold.d.fetch_sub(1, AcqRel) <= 0 {
                    return None;
                }
                break;
            }
        }
    }

    /// Get the number of usable entries in the ring buffer.
    const fn get_nr_entries() -> usize {
        1usize << ORDER
    }

    /// Get the number of cells in the ring buffer which is twice the
    /// number of usable entries.
    const fn get_nr_cells() -> usize {
        1usize << (ORDER + 1)
    }

    /// Map a cursor position to an index in the vector.
    const fn map(idx: usize, limit: usize, order: usize) -> usize {
        ((idx & (limit - 1)) >> (order - RING_MIN_ORDER)) |
	 ((idx << RING_MIN_ORDER) & (limit - 1))
    }

    /// Compute the value of the threshold bound.
    fn get_threshold(half: usize, nr: usize) -> isize {
	(half + nr - 1) as isize
    }

    /// Fixup the tail cursor so that it does not stay behind the
    /// head.
    fn catchup(&self, mut tail: usize, mut head: usize) {
        loop {
            if self.tail.d.compare_exchange_weak(tail, head, AcqRel, Acquire).is_ok() {
                return;
            }
            head = self.head.d.load(Acquire);
            tail = self.tail.d.load(Acquire);
            if sub_with_overflow(tail, head) >= 0 {
                return;
            }
        }
    }
}

/// The send-side of a ring queue. This side can only be owned by one
/// thread, but it can be cloned to send to other threads.
///
/// Messages can be sent to the associated FIFO through this channel
/// with [send()](crate::ring::Sender::send).
#[derive(Debug)]
pub struct Sender<T: Default, const ORDER: usize> {
    /// The associated FIFO to send messages to.
    rq: Arc<RingQueue<T, ORDER>>,
}

impl<T : Default, const ORDER: usize> Sender<T, ORDER> {
    /// Enqueues a message atomically into the associated FIFO,
    /// returning Some(()) if ok. Otherwise None is returned if the
    /// FIFO is full on entry to the call.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use std::sync::Arc;
    /// use std::thread as native_thread;
    /// use revl::thread::{self, *};
    /// use revl::sync::semaphore;
    /// use revl::sched::*;
    /// use revl::ring;
    ///
    /// // Create the send and receive sides of a ring queue.
    /// let (tx, rx) = ring::create::<i32, 16>();
    ///
    /// // Create a private, anonymous semaphore for synchronization.
    /// let sem = Arc::new(semaphore::Builder::new().create().unwrap());
    ///
    /// // Create the sender an receiver threads, with #0 sending to #1.
    /// for n in 0..2 {
    ///    let (tx, rx) = (tx.clone(), rx.clone());
    ///    let sem = sem.clone();
    ///    native_thread::spawn(move || {
    ///         let builder = thread::Builder::new()
    ///            .name(&format!("t{}-{}", n + 1, std::process::id()))
    ///            .sched(SchedAttrs::FIFO(1.into()));
    ///         // Attach the spawned thread to the EVL core.
    ///         Thread::attach(builder).unwrap();
    ///         loop {
    ///             if n == 0 {
    ///                assert_ne!(tx.send(42), None);
    ///                sem.put().unwrap();
    ///             } else {
    ///                sem.get().unwrap();
    ///                let msg = rx.recv().unwrap();
    ///                assert_eq!(msg, 42);
    ///             }
    ///         }
    ///    });
    ///  }
    /// ```
    pub fn send(&self, msg: T) -> Option<()> {
        self.rq.send(msg)
    }
}

impl<T: Default, const ORDER: usize> Clone for Sender<T, ORDER> {
    fn clone(&self) -> Self {
        Self { rq: self.rq.clone() }
    }
}

/// The receive-side of a ring queue. This side can only be owned by
/// one thread, but it can be cloned to send to other threads.
///
/// Messages can be received from the associated FIFO through this
/// channel with [recv()](crate::ring::Receiver::recv).
#[derive(Debug)]
pub struct Receiver<T: Default, const ORDER: usize> {
    /// The associated FIFO to receive messages from.
    rq: Arc<RingQueue<T, ORDER>>,
}

impl<T : Default, const ORDER: usize> Receiver<T, ORDER> {
    /// Pops and returns the leading message from the associated
    /// FIFO. None is returned if the FIFO is empty on entry to the
    /// call.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use std::sync::Arc;
    /// use std::thread as native_thread;
    /// use revl::thread::{self, *};
    /// use revl::sync::semaphore;
    /// use revl::sched::*;
    /// use revl::ring;
    ///
    /// // Create the send and receive sides of a ring queue.
    /// let (tx, rx) = ring::create::<i32, 16>();
    ///
    /// // Create a private, anonymous semaphore for synchronization.
    /// let sem = Arc::new(semaphore::Builder::new().create().unwrap());
    ///
    /// // Create the sender an receiver threads, with #0 sending to #1.
    /// for n in 0..2 {
    ///    let (tx, rx) = (tx.clone(), rx.clone());
    ///    let sem = sem.clone();
    ///    native_thread::spawn(move || {
    ///         let builder = thread::Builder::new()
    ///            .name(&format!("t{}-{}", n + 1, std::process::id()))
    ///            .sched(SchedAttrs::FIFO(1.into()));
    ///         // Attach the spawned thread to the EVL core.
    ///         Thread::attach(builder).unwrap();
    ///         loop {
    ///             if n == 0 {
    ///                assert_ne!(tx.send(42), None);
    ///                sem.put().unwrap();
    ///             } else {
    ///                sem.get().unwrap();
    ///                let msg = rx.recv().unwrap();
    ///                assert_eq!(msg, 42);
    ///             }
    ///         }
    ///    });
    ///  }
    /// ```
    pub fn recv(&self) -> Option<T> {
        self.rq.recv()
    }
}

impl<T: Default, const ORDER: usize> Clone for Receiver<T, ORDER> {
    fn clone(&self) -> Self {
        Self { rq: self.rq.clone() }
    }
}

#[derive(Debug)]
struct RingQueue<T: Default, const ORDER: usize> {
    /// Ring buffer containing indices pointing at busy cells into the
    /// data vector.
    ///
    /// Safe access to the UnsafeCell is guaranteed by the fact that
    /// at any point in time, only a single thread can refer to any
    /// given data cell, since the corresponding index in the vector
    /// is never shared (no W/W conflict), and a data cell cannot be
    /// consumed before it is fully populated with the message (no R/W
    /// conflict).
    dq: Ring::<ORDER>,
    /// Ring buffer containing indices pointing at free cells into the
    /// data vector.
    fq: Ring::<ORDER>,
    /// Vector containing the payload data.
    data: UnsafeCell<Vec<T>>,
}

impl<T : Default, const ORDER: usize> RingQueue<T, ORDER> {
    /// Push a message to the FIFO.
    ///
    /// Enqueues a message atomically into the FIFO, returning
    /// Some(()) if ok. Otherwise None is returned if the FIFO is full
    /// on entry to the call.
    fn send(&self, msg: T) -> Option<()> {
        if let Some(eidx) = self.fq.dequeue() {
            fence(Release);
            unsafe { (*self.data.get())[eidx] = msg; }
            // We have as many free slots than we have data cells, so
            // enqueuing cannot fail by construction.
            self.dq.enqueue(eidx);
            Some(())
        } else {
            None
        }
    }

    /// Receive a message from the FIFO.
    ///
    /// Pops and returns the leading message from the FIFO. Otherwise
    /// None is returned if the FIFO is empty on entry to the call.
    fn recv(&self) -> Option<T> {
        if let Some(eidx) = self.dq.dequeue() {
            // Make sure to take the message, releasing the cloned
            // references in the same move.
            let msg = unsafe { mem::take(&mut (*self.data.get())[eidx]) };
            fence(Acquire);
            self.fq.enqueue(eidx);
            Some(msg)
        } else {
            None
        }
    }
}

impl<T: Default, const ORDER: usize> Drop for RingQueue<T, ORDER> {
    fn drop(&mut self) {
        while let Some(eidx) = self.dq.dequeue() {
            unsafe { mem::take(&mut (*self.data.get())[eidx]) };
        }
    }
}

unsafe impl<T : Default + Send + Sync, const ORDER: usize> Sync for RingQueue<T, ORDER> {}

/// Create a ring queue.
///
/// Returns a tuple containing a sender and a receiver of messages of
/// type T to access the FIFO, which has `1 << ORDER` entries.
///
/// T must implement the Default trait.
///
/// # Examples
///
/// ```no_run
/// use revl::ring;
///
/// // Create the send and receive sides of a ring queue which can
/// // hold up to 1024 Foo messages concurrently.
///
/// #[derive(Default)]
/// struct Foo {
///    fpval: f64,
///    ival: u64,
/// }
///
/// let (tx, rx) = ring::create::<Foo, 1024>();
/// ```
pub fn create<T : Default + Send + Sync, const ORDER: usize>() -> (Sender<T, ORDER>, Receiver<T, ORDER>) {
    let nr_data = 1 << ORDER;
    let mut rq = RingQueue {
        dq: Ring::<ORDER>::new(),
        fq: Ring::<ORDER>::new(),
        data: UnsafeCell::new(Vec::with_capacity(nr_data)),
    };
    // Populate the data vector with default values, start with a full
    // free ring. Revisit: Until we have complex const generics
    // available, we need to allocate the vector separately.
    rq.data.get_mut().resize_with(nr_data, || { Default::default() });
    rq.fq.fill();
    let r = Arc::new(rq);
    ( Sender { rq: r.clone() }, Receiver { rq: r } )
}
