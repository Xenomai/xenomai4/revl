//! Rust interface to the EVL real-time core.
//!
//! This crate provides a Rust API to call the services of the
//! Xenomai4 [`real-time core`], aka EVL. The native EVL services are
//! invoked through the [`libevl`] C language library, via the
//! [`evl-sys`] FFI layer.
//!
//! For some applications, offloading a particular set of
//! time-critical tasks to a compact real-time core embedded into the
//! Linux kernel may deliver the best performance at the lowest
//! engineering and runtime costs in comparison to imposing real-time
//! behavior on the whole kernel logic in order to meet the deadlines
//! which only those tasks have, like the [`native preemption`] model
//! requires.
//! 
//! In a nutshell, EVL introduces a simple, scalable and dependable
//! dual kernel architecture for Linux, based on the [`Dovetail
//! interface`] for coupling a high-priority software core to the main
//! kernel.
//!
//! [`real-time core`]: https://evlproject.org/
//! [`native preemption`]: https://wiki.linuxfoundation.org/realtime/rtl/blog
//! [`Dovetail interface`]: https://evlproject.org/dovetail/
//! [`libevl`]: https://evlproject.org/core/user-api/
//! [`evl-sys`]: https://source.denx.de/Xenomai/xenomai4/evl-sys

pub mod clock;
pub mod ring;
pub mod sched;
pub mod sync;
pub mod thread;
pub mod proxy;
pub mod timer;
pub mod xbuf;

use std::io::{Error, ErrorKind};

/// Attach the calling process to the EVL core.
///
/// This call enables the EVL services for the current process.  Only
/// the first call to this routine applies, subsequent calls are
/// silently ignored, returning the status code of the initial call.
///
/// Attaching a thread to the EVL core implicitly enables the EVL
/// services for the calling process. However, in some cases we may
/// want to do this explicitly, like when initializing synchronization
/// objects prior to bootstrapping any real-time thread. This separate
/// call is provided for such purpose.
///
/// # Errors
///
/// [`ErrorKind::PermissionDenied`] is returned if the caller is not
/// allowed to lock memory via a call to [`mlockall(2)`]. Since memory
/// locking is a requirement for running EVL threads, no joy.
///
/// [`ErrorKind::OutOfMemory`] is returned if not enough memory is
/// available to complete the request, whether the kernel could not
/// lock down all of the calling process's virtual address space into
/// RAM, or some other reason related to some process or driver eating
/// way too much virtual or physical memory. Either way, you may start
/// panicking.
///
/// [`ErrorKind::Unsupported`] is returned if the EVL core is not enabled
/// in the running kernel.
///
/// [`ErrorKind::InvalidData`] denotes an ABI mismatch between the EVL
/// core in the running kernel and the underlying [`libevl`]
/// interface.  To fix this error, you need to [`rebuild libevl.so`]
/// against the UAPI exported by the EVL core running on the target
/// system.
///
/// [`ErrorKind::Other`] on unexpected error.
///
/// # Examples
///
/// ```no_run
/// use revl;
///
/// revl::init().unwrap();
/// ```
///
/// [`mlockall(2)`]: https://man7.org/linux/man-pages/man2/mlock.2.html
/// [`libevl`]: https://evlproject.org/core/build-steps/
/// [`rebuild libevl.so`]: https://evlproject.org/core/build-steps/
pub fn init() -> Result<(), Error> {
    // Trying to make the EVL bootstrap status fit into the skinny
    // std::io error set. Oh, well.
    match unsafe { evl_sys::evl_init() } {
        0.. => {
            Ok(())
        },
        e if -e == libc::EPERM => {
            Err(Error::from(ErrorKind::PermissionDenied))
        },
        e if -e == libc::ENOMEM => {
            Err(Error::from(ErrorKind::OutOfMemory))
        },
        e if -e == libc::ENOSYS => {
            Err(Error::from(ErrorKind::Unsupported))
        },
        e if -e == libc::ENOEXEC => {
            Err(Error::from(ErrorKind::InvalidData))
        },
        _ => {
            Err(Error::from(ErrorKind::Other))
        },
    }
}
