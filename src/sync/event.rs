//! Interface to EVL events.
//!
//! An event can be used for synchronizing EVL threads on the state of
//! some shared information in a raceless way. Proper serialization is
//! enforced by pairing an event with a [mutex](crate::sync::Mutex).
//! EVL events have the same semantics than POSIX threads.
//!
//! The Event struct is the Rust interface to the EVL [`event`]
//! synchronization object.
//!
//! [`event`]: https://evlproject.org/core/user-api/event/

use std::cell::UnsafeCell;
use std::ffi::CString;
use std::io::{Error, ErrorKind};
use std::mem::MaybeUninit;
use std::os::raw::{c_int, c_long};
use std::ptr;
use libc::{
    self,
    time_t,
};
use embedded_time::{
    duration::{Nanoseconds, Seconds},
    fixed_point::FixedPoint,
    Instant,
};
use evl_sys::{
    evl_event,
    evl_create_event,
    evl_close_event,
    evl_wait_event,
    evl_timedwait_event,
    evl_signal_event,
    evl_broadcast_event,
    evl_signal_thread,
    BuiltinClock,
    CloneFlags,
    timespec,
};
use crate::sync::MutexGuard;
use crate::clock::CoreClock;
use crate::thread::Thread;

/// An event builder contains properties and settings to be used for
/// creating a new event.
#[derive(Default, Debug)]
pub struct Builder {
    /// The event name. See [this
    /// document](https://evlproject.org/core/user-api/#element-naming-convention)
    /// for details about the naming convention.
    name: Option<String>,
    /// The core clock the timed services should use.
    clock: Option<CoreClock>,
    /// Whether this event is visible in the `/dev/evl` hierarchy.
    visible: bool,
}

impl Builder {
    /// Create an event builder.
    ///
    /// A default set of properties is used: anonymous event therefore
    /// not visible in the `/dev/evl` hierarchy, using the
    /// STEADY_CLOCK in timed operations.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::sync::event;
    ///
    /// let event = event::Builder::new()
    ///               .name(&format!("some_event-{}", std::process::id()))
    ///               .public()
    ///               .create();
    ///
    /// assert!(event.is_ok());
    /// ```
    ///
    /// [`STEADY_CLOCK`]: crate::clock::STEADY_CLOCK
    pub fn new() -> Self {
        Self {
            name: None,
            clock: None,
            visible: false,
        }
    }

    /// Set the name property. See [this
    /// document](https://evlproject.org/core/user-api/#element-naming-convention)
    /// for details about the naming convention.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::sync::event;
    ///
    /// let event = event::Builder::new()
    ///               .name(&format!("some_event-{}", std::process::id()))
    ///               .create();
    ///
    /// assert!(event.is_ok());
    /// ```
    pub fn name(mut self, name: &str) -> Self {
        self.name = Some(name.to_string());
        self
    }

    /// Set the visibility setting to `public`. See
    /// [this document](https://evlproject.org/core/user-api/#element-visibility)
    /// for details about element visibility.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::sync::event;
    ///
    /// let event = event::Builder::new()
    ///               .name(&format!("some_event-{}", std::process::id()))
    ///               .public()
    ///               .create();
    ///
    /// assert!(event.is_ok());
    /// ```
    pub fn public(mut self) -> Self {
        self.visible = true;
        self
    }

    /// Set the visibility setting to `private`. See
    /// [this document](https://evlproject.org/core/user-api/#element-visibility)
    /// for details about element visibility.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::sync::event;
    ///
    /// let event = event::Builder::new()
    ///               .private()
    ///               .create();
    ///
    /// assert!(event.is_ok());
    /// ```
    pub fn private(mut self) -> Self {
        self.visible = false;
        self
    }

    /// Set the reference [`clock`] the event should use. All timeouts
    /// passed to timed calls will be based on this clock.
    ///
    /// [`clock`]: crate::clock::CoreClock
    pub fn clock(mut self, clock: CoreClock) -> Self {
        self.clock = Some(clock);
        self
    }

    /// Create a new event out of this builder.
    ///
    /// # Errors
    ///
    /// Any error from [Event::new()](Event::new) may be returned.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::sync::event;
    ///
    /// let event = event::Builder::new()
    ///               .name(&format!("some_event-{}", std::process::id()))
    ///               .public()
    ///               .create();
    ///
    /// assert!(event.is_ok());
    /// ```
    pub fn create(self) -> Result<Event, Error> {
        Event::new(self)
    }
}

#[derive(Debug)]
pub struct WaitTimeoutResult(bool);

impl WaitTimeoutResult {
    #[must_use]
    pub fn timed_out(&self) -> bool {
        self.0
    }
}

/// An event synchronization object while allows for waiting for
/// notifications in a raceless way.
#[derive(Debug)]
pub struct Event(UnsafeCell<evl_event>);

unsafe impl Send for Event {}
unsafe impl Sync for Event {}

impl Event {
    /// Create a new EVL event. See [this
    /// document](https://evlproject.org/core/user-api/event/).
    ///
    /// # Errors
    ///
    /// [`ErrorKind::AlreadyExists`] if the generated name is
    /// conflicting with an existing mutex, event, semaphore or flag
    /// group name.
    ///
    /// [`ErrorKind::InvalidInput`] if the generated name is badly
    /// formed, or the overall length of the device element's file
    /// path including the generated name exceeds PATH_MAX.
    ///
    /// [`ErrorKind::OutOfMemory`] if not enough memory was available
    /// for completing the operation. By extension, this error status
    /// is also returned if the per-process limit on the number of
    /// open file descriptors or the system-wide limit on the total
    /// number of open files has been reached.
    ///
    /// [`ErrorKind::Other`] on unexpected error.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::sync::event::{self, Event};
    ///
    /// let builder = event::Builder::new()
    ///               .name(&format!("some_event-{}", std::process::id()))
    ///               .public();
    ///
    /// assert!(Event::new(builder).is_ok());
    /// ```
    pub fn new(builder: Builder) -> Result<Self, Error> {
        let this = Self(UnsafeCell::new(unsafe {
            MaybeUninit::<evl_event>::zeroed().assume_init()
        }));
        let mut c_flags = CloneFlags::PRIVATE.bits() as c_int;
        if builder.visible {
            c_flags = CloneFlags::PUBLIC.bits() as c_int;
        }
        let mut c_clockfd = BuiltinClock::MONOTONIC as i32;
        if let Some(clock) = builder.clock {
            c_clockfd = clock.0 as i32;
        }
        let ret: c_int = unsafe {
            if let Some(name) = builder.name {
                let c_name = CString::new(name).map_err(
                    |_| Error::from(ErrorKind::OutOfMemory)
                )?;
                let c_fmt = CString::new("%s").map_err(
                    |_| Error::from(ErrorKind::OutOfMemory)
                )?;
                evl_create_event(
                    this.0.get(),
                    c_clockfd,
                    c_flags,
                    c_fmt.as_ptr(),
                    c_name.as_ptr(),
                )
            } else {
                evl_create_event(this.0.get(),
                               c_clockfd,
                               c_flags,
                               ptr::null())
            }
        };
        match ret {
            0.. => {
                Ok(this)
            },
            e if -e == libc::EEXIST => {
                Err(Error::from(ErrorKind::AlreadyExists))
            },
            e if -e == libc::EINVAL || -e == libc::ENAMETOOLONG => {
                Err(Error::from(ErrorKind::InvalidInput))
            },
            e if -e == libc::ENOMEM || -e == libc::EMFILE || -e == libc::ENFILE => {
                Err(Error::from(ErrorKind::OutOfMemory))
            },
            _ => {
                Err(Error::from(ErrorKind::Other))
            },
        }
    }

    /// Wait for the condition associated with `self` to become
    /// true. Access to the condition must be protected by a [mutex
    /// guard](crate::sync::MutexGuard). Returns the guard on
    /// success.
    ///
    /// # Errors
    ///
    /// [`ErrorKind::Interrupted`] if the call was interrupted by an
    /// in-band signal, or forcibly unblocked by the EVL core.
    ///
    /// [`ErrorKind::PermissionDenied`] if the calling thread is not
    /// attached to the EVL core.
    ///
    /// [`ErrorKind::Other`] on unexpected error.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::sync::{event, mutex};
    ///
    /// let event = event::Builder::new()
    ///               .name(&format!("some_event-{}", std::process::id()))
    ///               .create().unwrap();
    ///
    /// let mutex = mutex::Builder::new()
    ///               .name(&format!("some_mutex-{}", std::process::id()))
    ///               .create(false).unwrap(); // Starts in !signaled state.
    ///
    /// let mut signaled = mutex.lock().unwrap();
    /// while !*signaled {
    ///     signaled = event.wait(signaled).unwrap();
    /// }
    /// ```
    pub fn wait<'a, T>(
        &self, guard: MutexGuard<'a, T>
    ) -> Result<MutexGuard<'a, T>, Error> {
        let ret: c_int = unsafe {
            evl_wait_event(self.0.get(), guard.as_raw_mut())
        };
        match -ret {
            0 => {
                Ok(guard)
            },
            libc::EINTR => {
                Err(Error::from(ErrorKind::Interrupted))
            },
            libc::EPERM => {
                Err(Error::from(ErrorKind::PermissionDenied))
            },
            _ => {
                Err(Error::from(ErrorKind::Other))
            },
        }
    }

    /// Wait for the condition associated with `self` to become true
    /// as long as the continuation `pred`icate yields true. Access to
    /// the condition must be protected by a [mutex
    /// guard](crate::sync::MutexGuard). Returns the guard on
    /// success. The predicate yielding false is considered a
    /// successful outcome of the operation.
    ///
    /// # Errors
    ///
    /// [`ErrorKind::Interrupted`] if the call was interrupted by an
    /// in-band signal, or forcibly unblocked by the EVL core.
    ///
    /// [`ErrorKind::PermissionDenied`] if the calling thread is not
    /// attached to the EVL core.
    ///
    /// [`ErrorKind::Other`] on unexpected error.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::sync::{event, mutex};
    ///
    /// let event = event::Builder::new()
    ///               .name(&format!("some_event-{}", std::process::id()))
    ///               .create().unwrap();
    ///
    /// let mutex = mutex::Builder::new()
    ///               .name(&format!("some_mutex-{}", std::process::id()))
    ///               .create(false).unwrap(); // Starts in !signaled state.
    ///
    /// let mut signaled = mutex.lock().unwrap();
    /// // As long as the value inside the `Mutex<bool>` is `true`, we wait.
    /// signaled = event.wait_while(signaled, |s| *s).unwrap();
    /// ```
    pub fn wait_while<'a, T, F>(
        &self,
        mut guard: MutexGuard<'a, T>,
        mut pred: F,
    ) -> Result<MutexGuard<'a, T>, Error>
    where
        F: FnMut(&mut T) -> bool,
    {
        while pred(&mut *guard) {
            guard = self.wait(guard)?;
        }
        Ok(guard)
    }

    /// Wait for the condition associated with `self` to become true
    /// until a `timeout` elapses. Access to the condition must be
    /// protected by a [mutex guard](crate::sync::MutexGuard). On
    /// success, returns a tuple containing the guard and a wait
    /// status which tells whether the condition was signaled on time
    /// or the timeout fired.
    ///
    /// # Errors
    ///
    /// [`ErrorKind::TimedOut`] if the timeout fired, after the amount
    /// of time specified by `timeout`.
    ///
    /// [`ErrorKind::Interrupted`] if the call was interrupted by an
    /// in-band signal, or forcibly unblocked by the EVL core.
    ///
    /// [`ErrorKind::PermissionDenied`] if the calling thread is not
    /// attached to the EVL core.
    ///
    /// [`ErrorKind::Other`] on unexpected error.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use embedded_time::{
    ///     duration::*,
    ///     Clock,
    ///     Instant,
    /// };
    /// use revl::clock::*;
    /// use revl::sync::{event, mutex};
    ///
    /// let event = event::Builder::new()
    ///               .name(&format!("some_event-{}", std::process::id()))
    ///               .create().unwrap();
    ///
    /// let mutex = mutex::Builder::new()
    ///               .name(&format!("some_mutex-{}", std::process::id()))
    ///               .create(false).unwrap(); // Starts in !signaled state.
    ///
    /// // Wait for at most one sec from now.
    /// let timeout = STEADY_CLOCK.now() + Seconds(1_u32);
    /// let mut signaled = mutex.lock().unwrap();
    /// loop {
    ///     let (guard, status) = event.wait_timed(signaled, timeout).unwrap();
    ///     if *guard {
    ///         break;  // Ok, signal received.
    ///     }
    ///     assert!(!status.timed_out()); // Assume any timeout is fatal.
    ///     signaled = guard;
    /// }
    /// ```
    pub fn wait_timed<'a, T>(
        &self,
        guard: MutexGuard<'a, T>,
        timeout: Instant::<CoreClock>,
    ) -> Result<(MutexGuard<'a, T>, WaitTimeoutResult), Error> {
        let dur = timeout.duration_since_epoch();
        let secs: Seconds<u64> = Seconds::try_from(dur).unwrap();
        let nsecs: Nanoseconds<u64> = Nanoseconds::<u64>::try_from(dur).unwrap() % secs;
        let date = timespec {
            tv_sec: secs.integer() as time_t,
            tv_nsec: nsecs.integer() as c_long,
        };
        let ret: c_int = unsafe {
            evl_timedwait_event(self.0.get(), guard.as_raw_mut(), &date)
        };
        match -ret {
            0 => {
                Ok((guard, WaitTimeoutResult(false)))
            },
            libc::ETIMEDOUT => {
                Ok((guard, WaitTimeoutResult(true)))
            },
            libc::EINTR => {
                Err(Error::from(ErrorKind::Interrupted))
            },
            libc::EPERM => {
                Err(Error::from(ErrorKind::PermissionDenied))
            },
            _ => {
                Err(Error::from(ErrorKind::Other))
            },
        }
    }

    /// Wait for the condition associated with `self` to become true
    /// until a `timeout` elapses or the continuation `pred`icate
    /// yields false, whichever comes first. Access to the condition
    /// must be protected by a [mutex
    /// guard](crate::sync::MutexGuard). On success, returns a tuple
    /// containing a guard and a wait status which tells whether the
    /// condition was signaled on time or, either the timeout fired or
    /// the predicate yielded false. The predicate yielding false is
    /// considered a successful outcome of the operation.
    ///
    /// # Errors
    ///
    /// [`ErrorKind::TimedOut`] if the timeout fired, after the amount
    /// of time specified by `timeout`.
    ///
    /// [`ErrorKind::Interrupted`] if the call was interrupted by an
    /// in-band signal, or forcibly unblocked by the EVL core.
    ///
    /// [`ErrorKind::PermissionDenied`] if the calling thread is not
    /// attached to the EVL core.
    ///
    /// [`ErrorKind::Other`] on unexpected error.
    /// # Examples
    ///
    /// ```no_run
    /// use embedded_time::{
    ///     duration::*,
    ///     Clock,
    ///     Instant,
    /// };
    /// use revl::clock::*;
    /// use revl::sync::{event, mutex};
    ///
    /// let event = event::Builder::new()
    ///               .name(&format!("some_event-{}", std::process::id()))
    ///               .create().unwrap();
    ///
    /// let mutex = mutex::Builder::new()
    ///               .name(&format!("some_mutex-{}", std::process::id()))
    ///               .create(false).unwrap(); // Starts in !signaled state.
    ///
    /// // Wait for at most one sec from now.
    /// let timeout = STEADY_CLOCK.now() + Seconds(1_u32);
    /// let mut signaled = mutex.lock().unwrap();
    /// // As long as the value inside the `Mutex<bool>` is `true` and
    /// // the timeout does not expire, we wait.
    /// let (signaled, status) = event.wait_timed_while(signaled, timeout, |s| *s).unwrap();
    /// assert!(!status.timed_out()); // Assume any timeout is fatal.
    /// ```
    pub fn wait_timed_while<'a, T, F>(
        &self,
        mut guard: MutexGuard<'a, T>,
        timeout: Instant::<CoreClock>,
        mut pred: F,
    ) -> Result<(MutexGuard<'a, T>, WaitTimeoutResult), Error>
    where F: FnMut(&mut T) -> bool
    {
        loop {
            if !pred(&mut *guard) {
                return Ok((guard, WaitTimeoutResult(false)));
            }
            let result = self.wait_timed(guard, timeout)?;
            if result.1.0 {
                return Ok(result);
            }
            guard = result.0;
        }
    }

    /// Notify a single waiter.
    ///
    /// This call notifies the thread heading the wait queue of an
    /// event at the time of the call (i.e. having the highest
    /// priority among waiters), indicating that the condition it has
    /// waited for may have become true.
    ///
    /// You **must** hold the mutex protecting accesses to the
    /// condition around the call to [notify_one()](Self::notify_one),
    /// because the notified thread will be actually readied when such
    /// lock is dropped, not at the time of the notification.
    ///
    /// This call should never fail.
    ///
    /// # Errors
    ///
    /// [`ErrorKind::Other`] on unexpected error.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::sync::{event, mutex};
    ///
    /// let event = event::Builder::new()
    ///               .name(&format!("some_event-{}", std::process::id()))
    ///               .create().unwrap();
    ///
    /// let mutex = mutex::Builder::new()
    ///               .name(&format!("some_mutex-{}", std::process::id()))
    ///               .create(false).unwrap(); // Starts in !signaled state.
    ///
    /// let mut signaled = mutex.lock().unwrap();
    /// *signaled = true;
    /// event.notify_one().unwrap();
    /// ```
    pub fn notify_one(&self) -> Result<(), Error> {
        let ret: c_int = unsafe { evl_signal_event(self.0.get()) };
        match ret {
            0 => {
                Ok(())
            },
            _ => {
                Err(Error::from(ErrorKind::Other))
            },
        }
    }

    /// Notify all waiters.
    ///
    /// A variant of [notify_one()](Self::notify_one) which notifies
    /// all waiters atomically.
    ///
    /// This call should never fail.
    ///
    /// # Errors
    ///
    /// [`ErrorKind::Other`] on unexpected error.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::sync::{event, mutex};
    ///
    /// let event = event::Builder::new()
    ///               .name(&format!("some_event-{}", std::process::id()))
    ///               .create().unwrap();
    ///
    /// let mutex = mutex::Builder::new()
    ///               .name(&format!("some_mutex-{}", std::process::id()))
    ///               .create(false).unwrap(); // Starts in !signaled state.
    ///
    /// let mut signaled = mutex.lock().unwrap();
    /// *signaled = true;
    /// event.notify_all().unwrap();
    /// ```
    pub fn notify_all(&self) -> Result<(), Error> {
        let ret: c_int = unsafe { evl_broadcast_event(self.0.get()) };
        match ret {
            0 => {
                Ok(())
            },
            _ => {
                Err(Error::from(ErrorKind::Other))
            },
        }
    }

    /// Notify a specific waiter.
    ///
    /// Yet another variant of [notify_one()](Self::notify_one) which
    /// notifies a particular `target` waiter.
    ///
    /// # Errors
    ///
    /// [`ErrorKind::BrokenPipe`] if the target thread is stale.
    ///
    /// [`ErrorKind::Other`] on unexpected error.
    ///
    /// ```no_run
    /// use std::sync::Arc;
    /// use std::thread as native_thread;
    /// use embedded_time::{
    ///     duration::*,
    ///     Clock,
    ///     Instant,
    /// };
    /// use revl::clock::*;
    /// use revl::sched::*;
    /// use revl::thread::{self, *};
    /// use revl::sync::{event, mutex};
    ///
    /// // Create a mutex which guards a floating point value.
    /// let fp = 0.0_f64;
    /// let mutex = Arc::new(mutex::Builder::new()
    ///               .name(&format!("some_mutex-{}", std::process::id()))
    ///               .public()
    ///               .create(fp).unwrap());
    ///
    /// // Create an event to synchronize on.
    /// let event = Arc::new(event::Builder::new()
    ///               .name(&format!("some_event-{}", std::process::id()))
    ///               .create().unwrap());
    ///
    /// // Clone the lock and condition to give to the updater.
    /// let lock = mutex.clone();
    /// let cond = event.clone();
    ///
    /// // Create the observer thread.
    /// native_thread::spawn(move || {
    ///     // Attach the observer to the EVL core.
    ///     let observer = Arc::new(
    ///        thread::Builder::new()
    ///           .name(&format!("observer-{}", std::process::id()))
    ///           .sched(SchedAttrs::FIFO(1.into()))
    ///           .attach()
    ///           .unwrap()
    ///     );
    ///
    ///     // Now create the updater thread from the observer context.
    ///     native_thread::spawn(move || {
    ///             // Attach the updater to the EVL core.
    ///             thread::Builder::new()
    ///                .name(&format!("updater-{}", std::process::id()))
    ///                .sched(SchedAttrs::FIFO(1.into()))
    ///                .attach()
    ///                .unwrap();
    ///
    ///             loop {
    ///                let mut guard = lock.lock().unwrap();
    ///                // Update fp.
    ///                *guard += 42.0;
    ///                // Wake up the observer specifically.
    ///                cond.notify_to(&observer).unwrap();
    ///                lock.unlock(guard); // i.e. drop(guard)
    ///                // Sleep 100 us to keep the kernel breathing.
    ///                let timeout = STEADY_CLOCK.now() + Microseconds(100_u32);
    ///                STEADY_CLOCK.sleep_until(timeout).unwrap();
    ///             }
    ///      });
    ///
    ///      let mut guard = mutex.lock().unwrap();
    ///      let mut prev = *guard;
    ///      // Observe changes to fp.
    ///      loop {
    ///            guard = event.wait(guard).unwrap();
    ///            if prev != *guard {
    ///               println!("value has changed: {} -> {}", prev, *guard);
    ///               prev = *guard;
    ///            }
    ///      }
    ///  });
    /// ```
    pub fn notify_to(&self, target: &Thread) -> Result<(), Error> {
        let ret: c_int = unsafe { evl_signal_thread(self.0.get(), target.0) };
        match -ret {
            0 => {
                Ok(())
            },
            libc::ESTALE => {
                // Well, cannot do better until ErrorKind maps more
                // unixish codes in -stable.
                Err(Error::from(ErrorKind::BrokenPipe))
            },
            _ => {
                Err(Error::from(ErrorKind::Other))
            },
        }
    }
}

impl Drop for Event {
    fn drop(&mut self) {
        unsafe {
            evl_close_event(self.0.get());
        }
    }
}

impl Default for Event {
    fn default() -> Self {
        Event::new(Builder::default()).unwrap()
    }
}
