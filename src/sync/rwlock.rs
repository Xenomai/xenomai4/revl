//! Interface to EVL read-write locks.
//!
//! EVL provides process-local read/write locks for serializing
//! threads accessing a shared resource from out-of-band context, with
//! multiple-reader, single writer semantics.
//!
//! The Rust implementation (naively) borrows from std::sync::RwLock -
//! without the poisoning feature - adapted to the libevl call
//! interface.

use std::cell::UnsafeCell;
use std::ptr::NonNull;
use std::io::{Error, ErrorKind};
use std::mem::MaybeUninit;
use std::ops::{Deref, DerefMut};
use std::os::raw::c_int;
use std::fmt;
use evl_sys::{
    evl_rwlock,
    evl_create_rwlock,
    evl_destroy_rwlock,
    evl_lock_read,
    evl_trylock_read,
    evl_unlock_read,
    evl_lock_write,
    evl_trylock_write,
    evl_unlock_write,
};

/// The `RwLock` struct implements a read/write lock with a bias
/// towards writers on acquiring such lock, since a waiting writer
/// shall be granted access before any new reader subsequently joining
/// the wait, even if the lock is owned by some reader at the time of
/// the request.
///
/// In other words, write-starvation cannot happen as a result of more
/// threads indefinitely joining the pool of current readers. Thread
/// priority is otherwise enforced among waiters for gaining access to
/// a free lock.
pub struct RwLock<T: ?Sized> {
    /// The core lock struct which wraps the EVL rwlock.
    inner: CoreRwLock,
    /// The inner data protected by the rwlock.
    data: UnsafeCell<T>,
}

unsafe impl<T: ?Sized + Send> Send for RwLock<T> {}
unsafe impl<T: ?Sized + Send> Sync for RwLock<T> {}

/// RAII structure used to release the shared read access of a lock
/// when dropped.
///
/// This structure is created by the [`read`](RwLock::read) and
/// [`try_read`](RwLock::try_read) methods on [`RwLock`].
pub struct RwLockReadGuard<'a, T: ?Sized + 'a> {
    data: NonNull<T>,
    inner_lock: &'a CoreRwLock,
}

unsafe impl<T: ?Sized + Sync> Sync for RwLockReadGuard<'_, T> {}

/// RAII structure used to release the exclusive write access of a
/// lock when dropped.
///
/// This structure is created by the [`read`](RwLock::write) and
/// [`try_write`](RwLock::try_write) methods on [`RwLock`].
pub struct RwLockWriteGuard<'a, T: ?Sized + 'a> {
    lock: &'a RwLock<T>,
}

unsafe impl<T: ?Sized + Sync> Sync for RwLockWriteGuard<'_, T> {}

impl<T> RwLock<T> {
    /// Create a new read/write lock for guarding `data`.
    ///
    /// # Panics
    ///
    /// This call may panic if not enough memory was available for
    /// completing the operation, or either of the per-process limit
    /// on the number of open file descriptors or the system-wide
    /// limit on the total number of open files has been reached.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::sync::RwLock;
    ///
    /// // Create a read/write lock which guards a tuple of integers.
    /// let tuple = (0, 0);
    /// let lock = RwLock::new(tuple);
    /// ```
    pub fn new(data: T) -> Self {
        Self {
            inner: CoreRwLock::new().unwrap(),
            data: UnsafeCell::new(data),
        }
    }
}

impl<T: ?Sized> RwLock<T> {
    /// Lock for reading. This call returns an RAII guard which
    /// guarantees shared read access to the inner data until such
    /// guard goes out of scope, releasing the lock. Alternatively,
    /// calling [`drop`] on the guard releases the lock too.
    ///
    /// # Errors
    ///
    /// [`ErrorKind::Interrupted`] if the call was interrupted by an
    /// in-band signal, or forcibly unblocked by the EVL core.
    ///
    /// [`ErrorKind::PermissionDenied`] if the calling thread is not
    /// attached to the EVL core.
    ///
    /// [`ErrorKind::Other`] on unexpected error.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use std::sync::Arc;
    /// use std::thread as native_thread;
    /// use embedded_time::{
    ///     duration::*,
    ///     Clock,
    ///     Instant,
    /// };
    /// use revl::clock::*;
    /// use revl::sched::*;
    /// use revl::thread::{self, *};
    /// use revl::sync::RwLock;
    ///
    /// // Create a read/write lock which guards a floating-point value.
    /// let fp = 0.0_f64;
    /// let lock = Arc::new(RwLock::new(fp));
    ///
    /// // Create a few threads reading and writing the value concurrently.
    /// for n in 1..=3 {
    ///    let lock = lock.clone();
    ///
    ///    native_thread::spawn(move || {
    ///         let builder = thread::Builder::new()
    ///            .name(&format!("t{}-{}", n, std::process::id()))
    ///            .sched(SchedAttrs::FIFO(1.into()));
    ///         // Attach the spawned thread to the EVL core.
    ///         Thread::attach(builder).unwrap();
    ///
    ///         loop {
    ///             if n == 0 { // The writer.
    ///                 let mut guard = lock.write().unwrap();
    ///                 *guard += 42.0;
    ///             } else { // The readers.
    ///                 let guard = lock.read().unwrap();
    ///                 println!("val = {}", *guard);
    ///             }
    ///             // Sleep 100 us to keep the kernel breathing.
    ///             let timeout = STEADY_CLOCK.now() + Microseconds(100_u32);
    ///             STEADY_CLOCK.sleep_until(timeout).unwrap();
    ///         }
    ///    });
    ///  }
    /// ```
    pub fn read(&self) -> Result<RwLockReadGuard<'_, T>, Error> {
        unsafe {
            self.inner.read_lock()?;
            Ok(RwLockReadGuard::new(self))
        }
    }

    /// Try locking for shared read access.
    ///
    /// If the access could not be granted immediately on entry, `Err`
    /// is returned.  Otherwise, an RAII guard is returned which will
    /// release the shared access when dropped.
    ///
    /// This call never blocks.
    ///
    /// Errors
    ///
    /// [`ErrorKind::WouldBlock`] is returned if the lock is already
    /// owned by a writer thread.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use std::sync::Arc;
    /// use std::thread as native_thread;
    /// use embedded_time::{
    ///     duration::*,
    ///     Clock,
    ///     Instant,
    /// };
    /// use revl::clock::*;
    /// use revl::sched::*;
    /// use revl::thread::{self, *};
    /// use revl::sync::RwLock;
    ///
    /// // Create a read/write lock which guards a floating-point value.
    /// let fp = 0.0_f64;
    /// let lock = Arc::new(RwLock::new(fp));
    ///
    /// // Create a few threads reading and writing the value concurrently.
    /// for n in 1..=3 {
    ///    let lock = lock.clone();
    ///
    ///    native_thread::spawn(move || {
    ///         let builder = thread::Builder::new()
    ///            .name(&format!("t{}-{}", n, std::process::id()))
    ///            .sched(SchedAttrs::FIFO(1.into()));
    ///         // Attach the spawned thread to the EVL core.
    ///         Thread::attach(builder).unwrap();
    ///
    ///         loop {
    ///             if n == 0 { // The writer.
    ///                 let mut guard = lock.write().unwrap();
    ///                 *guard += 42.0;
    ///             } else { // The readers.
    ///                 if let Ok(guard) = lock.try_read() {
    ///                     println!("val = {}", *guard);
    ///                 } else {
    ///                     println!("R/W conflict observed");
    ///                 }
    ///             }
    ///             // Sleep 100 us to keep the kernel breathing.
    ///             let timeout = STEADY_CLOCK.now() + Microseconds(100_u32);
    ///             STEADY_CLOCK.sleep_until(timeout).unwrap();
    ///         }
    ///    });
    ///  }
    /// ```
    pub fn try_read(&self) -> Result<RwLockReadGuard<'_, T>, Error> {
        unsafe {
            if self.inner.read_trylock() {
                Ok(RwLockReadGuard::new(self))
            } else {
                Err(ErrorKind::WouldBlock.into())
            }
        }
    }

    /// Lock for writing. This call returns an RAII guard which
    /// guarantees exclusive write access to the inner data until such
    /// guard goes out of scope, releasing the lock. Alternatively,
    /// calling [`drop`] on the guard releases the lock too.
    ///
    /// # Errors
    ///
    /// [`ErrorKind::Interrupted`] if the call was interrupted by an
    /// in-band signal, or forcibly unblocked by the EVL core.
    ///
    /// [`ErrorKind::PermissionDenied`] if the calling thread is not
    /// attached to the EVL core.
    ///
    /// [`ErrorKind::Other`] on unexpected error.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use std::sync::Arc;
    /// use std::thread as native_thread;
    /// use embedded_time::{
    ///     duration::*,
    ///     Clock,
    ///     Instant,
    /// };
    /// use revl::clock::*;
    /// use revl::sched::*;
    /// use revl::thread::{self, *};
    /// use revl::sync::RwLock;
    ///
    /// // Create a read/write lock which guards a floating-point value.
    /// let fp = 0.0_f64;
    /// let lock = Arc::new(RwLock::new(fp));
    ///
    /// // Create a few threads reading and writing the value concurrently.
    /// for n in 1..=3 {
    ///    let lock = lock.clone();
    ///
    ///    native_thread::spawn(move || {
    ///         let builder = thread::Builder::new()
    ///            .name(&format!("t{}-{}", n, std::process::id()))
    ///            .sched(SchedAttrs::FIFO(1.into()));
    ///         // Attach the spawned thread to the EVL core.
    ///         Thread::attach(builder).unwrap();
    ///
    ///         loop {
    ///             if n == 0 { // The writer.
    ///                 let mut guard = lock.write().unwrap();
    ///                 *guard += 42.0;
    ///             } else { // The readers.
    ///                 let guard = lock.read().unwrap();
    ///                 println!("val = {}", *guard);
    ///             }
    ///             // Sleep 100 us to keep the kernel breathing.
    ///             let timeout = STEADY_CLOCK.now() + Microseconds(100_u32);
    ///             STEADY_CLOCK.sleep_until(timeout).unwrap();
    ///         }
    ///    });
    ///  }
    /// ```
    pub fn write(&self) -> Result<RwLockWriteGuard<'_, T>, Error> {
        unsafe {
            self.inner.write_lock()?;
            Ok(RwLockWriteGuard::new(self))
        }
    }

    /// Try locking for exclusive write access.
    ///
    /// If the access could not be granted immediately on entry, `Err`
    /// is returned.  Otherwise, an RAII guard is returned which will
    /// release the shared access when dropped.
    ///
    /// This call never blocks.
    ///
    /// Errors
    ///
    /// [`ErrorKind::WouldBlock`] is returned if the lock is already
    /// owned by a thread.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use std::sync::Arc;
    /// use std::thread as native_thread;
    /// use embedded_time::{
    ///     duration::*,
    ///     Clock,
    ///     Instant,
    /// };
    /// use revl::clock::*;
    /// use revl::sched::*;
    /// use revl::thread::{self, *};
    /// use revl::sync::RwLock;
    ///
    /// // Create a read/write lock which guards a floating-point value.
    /// let fp = 0.0_f64;
    /// let lock = Arc::new(RwLock::new(fp));
    ///
    /// // Create a few threads reading and writing the value concurrently.
    /// for n in 1..=3 {
    ///    let lock = lock.clone();
    ///
    ///    native_thread::spawn(move || {
    ///         let builder = thread::Builder::new()
    ///            .name(&format!("t{}-{}", n, std::process::id()))
    ///            .sched(SchedAttrs::FIFO(1.into()));
    ///         // Attach the spawned thread to the EVL core.
    ///         Thread::attach(builder).unwrap();
    ///
    ///         loop {
    ///             if n > 0 { // The readers.
    ///                 let guard = lock.read().unwrap();
    ///                 println!("val = {}", *guard);
    ///             } else { // The writer.
    ///                 if let Ok(mut guard) = lock.try_write() {
    ///                     *guard += 42.0;
    ///                 } else {
    ///                     println!("R/W or W/W conflict observed");
    ///                 }
    ///             }
    ///             // Sleep 100 us to keep the kernel breathing.
    ///             let timeout = STEADY_CLOCK.now() + Microseconds(100_u32);
    ///             STEADY_CLOCK.sleep_until(timeout).unwrap();
    ///         }
    ///    });
    ///  }
    /// ```
    pub fn try_write(&self) -> Result<RwLockWriteGuard<'_, T>, Error> {
        unsafe {
            if self.inner.write_trylock() {
                Ok(RwLockWriteGuard::new(self))
            } else {
                Err(ErrorKind::WouldBlock.into())
            }
        }
    }

    /// Consume the lock, returning the inner data.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::sync::RwLock;
    ///
    /// #[derive(Debug, PartialEq)]
    /// struct Foo {
    ///    fpval: f64,
    ///    ival: u64,
    /// }
    ///
    /// // Create a rwlock which guards a struct Foo instance.
    /// let lock = RwLock::new(Foo { fpval: 42.0, ival: 5150 });
    ///
    /// assert_eq!(lock.into_inner(), Foo { fpval: 42.0, ival: 5150  });
    /// ```
    pub fn into_inner(self) -> T
    where T: Sized,
    {
        self.data.into_inner()
    }

    /// Return a mutable reference to the underlying data.
    ///
    /// The mutable borrow statically guarantees the caller exclusive
    /// access to the inner data, therefore no locking needs to take
    /// place.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::sync::RwLock;
    ///
    /// #[derive(Debug, PartialEq)]
    /// struct Foo {
    ///    fpval: f64,
    ///    ival: u64,
    /// }
    ///
    /// // Create a rwlock which guards a struct Foo instance.
    /// let mut lock = RwLock::new(Foo { fpval: 42.0, ival: 5150 });
    /// lock.get_mut().fpval += 1.0;
    /// lock.get_mut().ival += 5;
    /// assert_eq!(lock.into_inner(), Foo { fpval: 43.0, ival: 5155 });
    /// ```
    pub fn get_mut(&mut self) -> &mut T {
        self.data.get_mut()
    }
}

impl<T: ?Sized> fmt::Debug for RwLock<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "RwLock address: {:?}", &self.inner as *const _)
    }
}

impl<T: Default> Default for RwLock<T> {
    /// Create a new `RwLock<T>`, with the `Default` value for T.
    fn default() -> RwLock<T> {
        RwLock::new(Default::default())
    }
}

impl<T> From<T> for RwLock<T> {
    /// Create a new instance of an `RwLock<T>` which is unlocked.
    /// This is equivalent to [`RwLock::new`].
    fn from(t: T) -> Self {
        RwLock::new(t)
    }
}

impl<'rwlock, T: ?Sized> RwLockReadGuard<'rwlock, T> {
    /// Create a new instance of `RwLockReadGuard<T>` from a `RwLock<T>`.
    unsafe fn new(lock: &'rwlock RwLock<T>) -> RwLockReadGuard<'rwlock, T> {
        RwLockReadGuard {
            data: NonNull::new_unchecked(lock.data.get()),
            inner_lock: &lock.inner,
        }
    }
}

impl<T: fmt::Debug> fmt::Debug for RwLockReadGuard<'_, T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        (**self).fmt(f)
    }
}

impl<T: ?Sized + fmt::Display> fmt::Display for RwLockReadGuard<'_, T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        (**self).fmt(f)
    }
}

impl<T: ?Sized> Deref for RwLockReadGuard<'_, T> {
    type Target = T;

    fn deref(&self) -> &T {
        unsafe { self.data.as_ref() }
    }
}

impl<T: ?Sized> Drop for RwLockReadGuard<'_, T> {
    fn drop(&mut self) {
        self.inner_lock.read_unlock().ok();
    }
}

impl<'rwlock, T: ?Sized> RwLockWriteGuard<'rwlock, T> {
    /// Create a new instance of `RwLockWriteGuard<T>` from a `RwLock<T>`.
    unsafe fn new(lock: &'rwlock RwLock<T>) -> RwLockWriteGuard<'rwlock, T> {
        RwLockWriteGuard { lock }
    }
}

impl<T: fmt::Debug> fmt::Debug for RwLockWriteGuard<'_, T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        (**self).fmt(f)
    }
}

impl<T: ?Sized + fmt::Display> fmt::Display for RwLockWriteGuard<'_, T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        (**self).fmt(f)
    }
}

impl<T: ?Sized> Deref for RwLockWriteGuard<'_, T> {
    type Target = T;

    fn deref(&self) -> &T {
        unsafe { &*self.lock.data.get() }
    }
}

impl<T: ?Sized> DerefMut for RwLockWriteGuard<'_, T> {
    fn deref_mut(&mut self) -> &mut T {
        unsafe { &mut *self.lock.data.get() }
    }
}

impl<T: ?Sized> Drop for RwLockWriteGuard<'_, T> {
    fn drop(&mut self) {
        self.lock.inner.write_unlock().ok();
    }
}

struct CoreRwLock(UnsafeCell<evl_rwlock>);

impl Drop for CoreRwLock {
    fn drop(&mut self) {
        unsafe {
            evl_destroy_rwlock(self.0.get());
        }
    }
}

impl fmt::Debug for CoreRwLock {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", &self.0 as *const _)
    }
}

impl CoreRwLock {
    /// Internal call to create an EVL rwlock. The new lock is
    /// unlocked.
    fn new() -> Result<Self, Error> {
        let this = Self(UnsafeCell::new(unsafe {
            MaybeUninit::<evl_rwlock>::zeroed().assume_init()
        }));
        let ret: c_int = unsafe {
            evl_create_rwlock(this.0.get())
        };
        match ret {
            0.. => {
                Ok(this)
            },
            e if -e == libc::ENOMEM || -e == libc::EMFILE || -e == libc::ENFILE => {
                Err(Error::from(ErrorKind::OutOfMemory))
            },
            _ => {
                Err(Error::from(ErrorKind::Other))
            },
        }
    }

    /// Internal call to get a shared access to the data protected by
    /// the EVL rwlock.
    fn read_lock(&self) -> Result<(), Error> {
        let ret: c_int = unsafe { evl_lock_read(self.0.get()) };
        match -ret {
            0 => {
                Ok(())
            },
            libc::EPERM => {
                Err(Error::from(ErrorKind::PermissionDenied))
            },
            _ => {
                Err(Error::from(ErrorKind::Other))
            },
        }
    }

    /// Internal call to attempt to try locking the EVL rwlock for
    /// gaining shared access to the protected inner data. Returns
    /// true on success.
    fn read_trylock(&self) -> bool {
        let ret: c_int = unsafe { evl_trylock_read(self.0.get()) };
        matches!(ret, 0)
    }

    /// Internal call to unlock an EVL rwlock previously locked for
    /// shared read access.
    fn read_unlock(&self) -> Result<(), Error> {
        let ret: c_int = unsafe { evl_unlock_read(self.0.get()) };
        match ret {
            0 => {
                Ok(())
            },
            _ => {
                Err(Error::from(ErrorKind::Other))
            },
        }
    }

    /// Internal call to get an exclusive access to the data protected
    /// by the EVL rwlock.
    fn write_lock(&self) -> Result<(), Error> {
        let ret: c_int = unsafe { evl_lock_write(self.0.get()) };
        match -ret {
            0 => {
                Ok(())
            },
            libc::EPERM => {
                Err(Error::from(ErrorKind::PermissionDenied))
            },
            _ => {
                Err(Error::from(ErrorKind::Other))
            },
        }
    }

    /// Internal call to attempt to try locking the EVL rwlock for
    /// gaining exclusive access to the protected inner data. Returns
    /// true on success.
    fn write_trylock(&self) -> bool {
        let ret: c_int = unsafe { evl_trylock_write(self.0.get()) };
        matches!(ret, 0)
    }

    /// Internal call to unlock an EVL rwlock previously locked for
    /// exclusive write access.
    fn write_unlock(&self) -> Result<(), Error> {
        let ret: c_int = unsafe { evl_unlock_write(self.0.get()) };
        match -ret {
            0 => {
                Ok(())
            },
            _ => {
                Err(Error::from(ErrorKind::Other))
            },
        }
    }
}
