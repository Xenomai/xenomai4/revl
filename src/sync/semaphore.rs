//! Interface to EVL counting semaphores.
//!
//! EVL provides common counting semaphores for thread synchronization
//! which may be shared between processes.
//!
//! The Semaphore struct is the Rust interface to the EVL
//! [`semaphore`] object.
//!
//! [`semaphore`]: https://evlproject.org/core/user-api/semaphore/

use std::cell::UnsafeCell;
use std::ffi::CString;
use std::io::{Error, ErrorKind};
use std::mem::MaybeUninit;
use std::os::raw::{c_int, c_long};
use std::ptr;
use libc::time_t;
use embedded_time::{
    duration::{Nanoseconds, Seconds},
    fixed_point::FixedPoint,
    Instant,
};
use evl_sys::{
    evl_close_sem,
    evl_create_sem,
    evl_get_sem,
    evl_timedget_sem,
    evl_tryget_sem,
    evl_peek_sem,
    evl_put_sem,
    evl_flush_sem,
    evl_sem,
    BuiltinClock,
    CloneFlags,
    timespec,
};
use crate::clock::CoreClock;

/// An event builder contains properties and settings to be used for
/// creating a new semaphore.
#[derive(Default, Debug)]
pub struct Builder {
    /// The semaphore name. See [this
    /// document](https://evlproject.org/core/user-api/#element-naming-convention)
    /// for details about the naming convention.
    name: Option<String>,
    /// The core clock the timed services should use.
    clock: Option<CoreClock>,
    /// Whether this semaphore is visible in the `/dev/evl` hierarchy.
    visible: bool,
    /// Whether threads should never block for decrementing the
    /// semaphore.
    nonblock: bool,
    /// The initial value of the semaphore.
    initval: u32,
}

impl Builder {
    /// Create a semaphore builder.
    ///
    /// A default set of properties is used: anonymous semaphore,
    /// therefore not visible in the `/dev/evl` hierarchy, blocking on
    /// decrement operations, initialized to zero.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::sync::semaphore;
    ///
    /// // Create a publicly visible semaphore.
    /// let sem = semaphore::Builder::new()
    ///               .name(&format!("some_sem-{}", std::process::id()))
    ///               .public()
    ///               .create();
    ///
    /// assert!(sem.is_ok());
    /// ```
    pub fn new() -> Self {
        Self {
            name: None,
            clock: None,
            visible: false,
            initval: 0u32,
            nonblock: false,
        }
    }

    /// Set the name property. See [this
    /// document](https://evlproject.org/core/user-api/#element-naming-convention)
    /// for details about the naming convention.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::sync::semaphore;
    ///
    /// // Create a named semaphore.
    /// let sem = semaphore::Builder::new()
    ///               .name(&format!("some_sem-{}", std::process::id()))
    ///               .create();
    ///
    /// assert!(sem.is_ok());
    /// ```
    pub fn name(mut self, name: &str) -> Self {
        self.name = Some(name.to_string());
        self
    }

    /// Set the reference [`clock`] the semaphore should use. All
    /// timeouts passed to timed calls will be based on this clock.
    ///
    /// [`clock`]: crate::clock::CoreClock
    pub fn clock(mut self, clock: CoreClock) -> Self {
        self.clock = Some(clock);
        self
    }

    /// Set the visibility setting to `public`. See
    /// [this document](https://evlproject.org/core/user-api/#element-visibility)
    /// for details about element visibility.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::sync::semaphore;
    ///
    /// // Create a publicly visible semaphore.
    /// let sem = semaphore::Builder::new()
    ///               .name(&format!("some_sem-{}", std::process::id()))
    ///               .public()
    ///               .create();
    ///
    /// assert!(sem.is_ok());
    /// ```
    pub fn public(mut self) -> Self {
        self.visible = true;
        self
    }

    /// Set the visibility setting to `private`. See
    /// [this document](https://evlproject.org/core/user-api/#element-visibility)
    /// for details about element visibility.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::sync::semaphore;
    ///
    /// // Create a private semaphore.
    /// let sem = semaphore::Builder::new()
    ///               .private()
    ///               .create();
    ///
    /// assert!(sem.is_ok());
    /// ```
    pub fn private(mut self) -> Self {
        self.visible = false;
        self
    }

    /// Set the initial value of the semaphore to `initval`.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::sync::semaphore;
    ///
    /// // Create a semaphore with a count initialized to 16.
    /// let sem = semaphore::Builder::new()
    ///               .init_value(16)
    ///               .create();
    ///
    /// assert!(sem.is_ok());
    /// ```
    pub fn init_value(mut self, initval: u32) -> Self {
        self.initval = initval;
        self
    }

    /// Turn on non-blocking mode.
    ///
    /// Use this property to prevent threads from blocking in
    /// decrement operations, receiving the `ErrorKind::WouldBlock`
    /// error status instead.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::sync::semaphore;
    ///
    /// let sem = semaphore::Builder::new()
    ///               .nonblock()
    ///               .create();
    ///
    /// assert!(sem.is_ok());
    /// ```
    pub fn nonblock(mut self) -> Self {
        self.nonblock = true;
        self
    }

    /// Create a new semaphore out of this builder.
    ///
    /// # Errors
    ///
    /// Any error from [Semaphore::new()](Semaphore::new) may be
    /// returned.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::sync::semaphore;
    ///
    /// // Create a publicly visible semaphore.
    /// let sem = semaphore::Builder::new()
    ///               .name(&format!("some_sem-{}", std::process::id()))
    ///               .public()
    ///               .create();
    ///
    /// assert!(sem.is_ok());
    /// ```
    pub fn create(self) -> Result<Semaphore, Error> {
        Semaphore::new(self)
    }
}

#[derive(Debug)]
pub struct Semaphore(UnsafeCell<evl_sem>);

unsafe impl Send for Semaphore {}
unsafe impl Sync for Semaphore {}

impl Semaphore {
    /// Create an EVL semaphore, retrieving the settings from a
    /// [`builder struct`](Builder).
    ///
    /// # Errors
    ///
    /// [`ErrorKind::AlreadyExists`] if the generated name is
    /// conflicting with an existing mutex, event, semaphore or flag
    /// group name.
    ///
    /// [`ErrorKind::InvalidInput`] if the generated name is badly
    /// formed, or the overall length of the device element's file
    /// path including the generated name exceeds PATH_MAX.
    ///
    /// [`ErrorKind::OutOfMemory`] if not enough memory was available
    /// for completing the operation. By extension, this error status
    /// is also returned if the per-process limit on the number of
    /// open file descriptors or the system-wide limit on the total
    /// number of open files has been reached.
    ///
    /// [`ErrorKind::Other`] on unexpected error.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::sync::semaphore::{self, Semaphore};
    ///
    /// let builder = semaphore::Builder::new()
    ///               .name(&format!("some_sem-{}", std::process::id()))
    ///               .public();
    ///
    /// assert!(Semaphore::new(builder).is_ok());
    /// ```
    pub fn new(builder: Builder) -> Result<Self, Error> {
        let this = Self(UnsafeCell::new(unsafe {
            MaybeUninit::<evl_sem>::zeroed().assume_init()
        }));
        let mut c_flags = CloneFlags::PRIVATE.bits() as c_int;
        if builder.visible {
            c_flags = CloneFlags::PUBLIC.bits() as c_int;
        }
        if builder.nonblock {
            c_flags |= CloneFlags::NONBLOCK.bits() as c_int;
        }
        let c_initval = builder.initval as i32;
        let mut c_clockfd = BuiltinClock::MONOTONIC as i32;
        if let Some(clock) = builder.clock {
            c_clockfd = clock.0 as i32;
        }
        let ret: c_int = unsafe {
            if let Some(name) = builder.name {
                let c_name = CString::new(name).map_err(
                    |_| Error::from(ErrorKind::OutOfMemory)
                )?;
                let c_fmt = CString::new("%s").map_err(
                    |_| Error::from(ErrorKind::OutOfMemory)
                )?;
                evl_create_sem(
                    this.0.get(),
                    c_clockfd,
                    c_initval,
                    c_flags,
                    c_fmt.as_ptr(),
                    c_name.as_ptr(),
                )
            } else {
                evl_create_sem(this.0.get(),
                               c_clockfd,
                               c_initval,
                               c_flags,
                               ptr::null())
            }
        };
        match ret {
            0.. => {
                Ok(this)
            },
            e if -e == libc::EEXIST => {
                Err(Error::from(ErrorKind::AlreadyExists))
            },
            e if -e == libc::EINVAL || -e == libc::ENAMETOOLONG => {
                Err(Error::from(ErrorKind::InvalidInput))
            },
            e if -e == libc::ENOMEM || -e == libc::EMFILE || -e == libc::ENFILE => {
                Err(Error::from(ErrorKind::OutOfMemory))
            },
            _ => {
                Err(Error::from(ErrorKind::Other))
            },
        }
    }

    /// Decrement a semaphore by one.
    ///
    /// If the resulting semaphore value is negative, the caller is
    /// blocked until a converse call to [put()](Self::put)
    /// eventually releases it, otherwise, the caller returns
    /// immediately.  Waiters are queued by order of scheduling
    /// priority.
    ///
    /// # Errors
    ///
    /// [`ErrorKind::Interrupted`] if the call was interrupted by an
    /// in-band signal, or forcibly unblocked by the EVL core.
    ///
    /// [`ErrorKind::WouldBlock`] if non-blocking mode is enabled for
    /// the semaphore which count is zero or less on entry to the
    /// call.
    ///
    /// [`ErrorKind::PermissionDenied`] if the calling thread is not
    /// attached to the EVL core.
    ///
    /// [`ErrorKind::Other`] on unexpected error.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use std::sync::Arc;
    /// use std::thread as native_thread;
    /// use revl::thread::{self, *};
    /// use revl::sched::*;
    /// use revl::sync::semaphore;
    ///
    /// // Create a private, anonymous semaphore for synchronization.
    /// let sem = Arc::new(semaphore::Builder::new().create().unwrap());
    ///
    /// // Create the poster an waiter threads, with #0 notifying #1.
    /// for n in 0..2 {
    ///    let sem = sem.clone();
    ///    native_thread::spawn(move || {
    ///         let builder = thread::Builder::new()
    ///            .name(&format!("t{}-{}", n + 1, std::process::id()))
    ///            .sched(SchedAttrs::FIFO(1.into()));
    ///         // Attach the spawned thread to the EVL core.
    ///         Thread::attach(builder).unwrap();
    ///         loop {
    ///             if n == 0 {
    ///                sem.put().unwrap();
    ///             } else {
    ///                sem.get().unwrap();
    ///             }
    ///         }
    ///    });
    ///  }
    /// ```
    pub fn get(&self) -> Result<(), Error> {
        let ret: c_int = unsafe {
            evl_get_sem(self.0.get())
        };
        match -ret {
            0 => {
                Ok(())
            },
            libc::EINTR => {
                Err(Error::from(ErrorKind::Interrupted))
            },
            libc::EAGAIN => {
                Err(Error::from(ErrorKind::WouldBlock))
            },
            libc::EPERM => {
                Err(Error::from(ErrorKind::PermissionDenied))
            },
            _ => {
                Err(Error::from(ErrorKind::Other))
            },
        }
    }

    /// Decrement a semaphore by one with timed wait.
    ///
    /// This call is a variable of [get()](Self::get) which accepts a
    /// time limit for waiting. If non-blocking mode is enabled for
    /// the semaphore, the timeout is ignored.
    ///
    /// # Errors
    ///
    /// [`ErrorKind::TimedOut`] if the timeout fired, after the amount
    /// of time specified by `timeout`.
    ///
    /// [`ErrorKind::WouldBlock`] if non-blocking mode is enabled for
    /// the semaphore which count is zero or less on entry to the
    /// call.
    ///
    /// [`ErrorKind::Interrupted`] if the call was interrupted by an
    /// in-band signal, or forcibly unblocked by the EVL core.
    ///
    /// [`ErrorKind::PermissionDenied`] if the calling thread is not
    /// attached to the EVL core.
    ///
    /// [`ErrorKind::Other`] on unexpected error.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use std::sync::Arc;
    /// use std::thread as native_thread;
    /// use embedded_time::{
    ///     duration::*,
    ///     Clock,
    ///     Instant,
    /// };
    /// use revl::clock::*;
    /// use revl::thread::{self, *};
    /// use revl::sched::*;
    /// use revl::sync::semaphore;
    ///
    /// // Create a private, anonymous semaphore for synchronization.
    /// let sem = Arc::new(semaphore::Builder::new().create().unwrap());
    ///
    /// // Create the poster an waiter threads, with #0 notifying #1.
    /// for n in 0..2 {
    ///    let sem = sem.clone();
    ///    native_thread::spawn(move || {
    ///         let builder = thread::Builder::new()
    ///            .name(&format!("t{}-{}", n + 1, std::process::id()))
    ///            .sched(SchedAttrs::FIFO(1.into()));
    ///         // Attach the spawned thread to the EVL core.
    ///         Thread::attach(builder).unwrap();
    ///         loop {
    ///             if n == 0 {
    ///                sem.put().unwrap();
    ///                let timeout = STEADY_CLOCK.now() + Microseconds(200_u32);
    ///                STEADY_CLOCK.sleep_until(timeout).unwrap();
    ///             } else {
    ///                let timeout = STEADY_CLOCK.now() + Microseconds(200_u32);
    ///                sem.get_timed(timeout).unwrap();
    ///             }
    ///         }
    ///    });
    ///  }
    /// ```
    pub fn get_timed(
        &self, timeout: Instant::<CoreClock>
    ) -> Result<(), Error> {
        let dur = timeout.duration_since_epoch();
        let secs: Seconds<u64> = Seconds::try_from(dur).unwrap();
        let nsecs: Nanoseconds<u64> = Nanoseconds::<u64>::try_from(dur).unwrap() % secs;
        let date = timespec {
            tv_sec: secs.integer() as time_t,
            tv_nsec: nsecs.integer() as c_long,
        };
        let ret: c_int = unsafe {
            evl_timedget_sem(self.0.get(), &date)
        };
        match -ret {
            0 => {
                Ok(())
            },
            libc::ETIMEDOUT => {
                Err(Error::from(ErrorKind::TimedOut))
            },
            libc::EINTR => {
                Err(Error::from(ErrorKind::Interrupted))
            },
            libc::EAGAIN => {
                Err(Error::from(ErrorKind::WouldBlock))
            },
            libc::EPERM => {
                Err(Error::from(ErrorKind::PermissionDenied))
            },
            _ => {
                Err(Error::from(ErrorKind::Other))
            },
        }
    }

    /// Attempt to decrement a semaphore.
    ///
    /// This call attempts to decrement the semaphore provided the
    /// result does not yield a negative count, returning true on
    /// success. Otherwise, the routine immediately returns false.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use std::sync::Arc;
    /// use std::thread as native_thread;
    /// use embedded_time::{
    ///     duration::*,
    ///     Clock,
    ///     Instant,
    /// };
    /// use revl::clock::*;
    /// use revl::thread::{self, *};
    /// use revl::sched::*;
    /// use revl::sync::semaphore;
    ///
    /// // Create a private, anonymous semaphore for synchronization.
    /// let sem = Arc::new(semaphore::Builder::new().create().unwrap());
    ///
    /// // Create the poster an waiter threads, with #0 notifying #1.
    /// for n in 0..2 {
    ///    let sem = sem.clone();
    ///    native_thread::spawn(move || {
    ///         let builder = thread::Builder::new()
    ///            .name(&format!("t{}-{}", n + 1, std::process::id()))
    ///            .sched(SchedAttrs::FIFO(1.into()));
    ///         // Attach the spawned thread to the EVL core.
    ///         Thread::attach(builder).unwrap();
    ///         loop {
    ///             if n == 0 {
    ///                sem.put().unwrap();
    ///                let timeout = STEADY_CLOCK.now() + Seconds(1_u32);
    ///                STEADY_CLOCK.sleep_until(timeout).unwrap();
    ///             } else {
    ///                if !sem.try_get() {
    ///                   println!("semaphore is depleted");
    ///                } else {
    ///                   println!("semaphore granted");
    ///               }
    ///                let timeout = STEADY_CLOCK.now() + Microseconds(300_u32);
    ///                STEADY_CLOCK.sleep_until(timeout).unwrap();
    ///             }
    ///         }
    ///    });
    ///  }
    /// ```
    pub fn try_get(&self) -> bool {
        let ret: c_int = unsafe {
            evl_tryget_sem(self.0.get())
        };
        matches!(ret, 0)
    }

    /// Query the semaphore value.
    ///
    /// This call returns the count value of the semaphore on
    /// success. If a negative count is returned, its absolute value
    /// can be interpreted as the number of waiters blocked on the
    /// semaphore's wait queue at the time of the call. Zero or any
    /// positive value means that the semaphore is not contended.
    ///
    /// # Errors
    ///
    /// [`ErrorKind::Other`] on unexpected error.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::sync::semaphore;
    ///
    /// let sem = semaphore::Builder::new()
    ///               .name(&format!("some_sem-{}", std::process::id()))
    ///               .public()
    ///               .init_value(1024)
    ///               .create().unwrap();
    ///
    /// // Nobody could have possibly accessed this semaphore yet.
    /// let res = sem.peek().unwrap();
    /// assert_eq!(res, 1024);
    /// ```
    pub fn peek(&self) -> Result<i32, Error> {
	let mut val = MaybeUninit::<i32>::uninit();
        let ret: c_int = unsafe {
            evl_peek_sem(self.0.get(), val.as_mut_ptr())
        };
        match ret {
            0 => {
                Ok(unsafe { val.assume_init() })
            },
            _ => {
                Err(Error::from(ErrorKind::Other))
            }
        }
    }

    /// Increment a semaphore by one.
    ///
    /// This call posts a semaphore, releasing a resource. If some
    /// thread is currently blocked on the semaphore by
    /// [get()](Self::get) or [get_timed()](Self::get_timed), the one
    /// leading the wait queue is unblocked. Otherwise, the semaphore
    /// count is incremented by one.
    ///
    /// # Errors
    ///
    /// [`ErrorKind::Other`] on unexpected error.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use std::sync::Arc;
    /// use std::thread as native_thread;
    /// use revl::thread::{self, *};
    /// use revl::sched::*;
    /// use revl::sync::semaphore;
    ///
    /// // Create a private, anonymous semaphore for synchronization.
    /// let sem = Arc::new(semaphore::Builder::new().create().unwrap());
    ///
    /// // Create the poster an waiter threads, with #0 notifying #1.
    /// for n in 0..2 {
    ///    let sem = sem.clone();
    ///    native_thread::spawn(move || {
    ///         let builder = thread::Builder::new()
    ///            .name(&format!("t{}-{}", n + 1, std::process::id()))
    ///            .sched(SchedAttrs::FIFO(1.into()));
    ///         // Attach the spawned thread to the EVL core.
    ///         Thread::attach(builder).unwrap();
    ///         loop {
    ///             if n == 0 {
    ///                sem.put().unwrap();
    ///             } else {
    ///                sem.get().unwrap();
    ///             }
    ///         }
    ///    });
    ///  }
    /// ```
    pub fn put(&self) -> Result<(), Error> {
        let ret: c_int = unsafe {
            evl_put_sem(self.0.get())
        };
        match ret {
            0 => {
                Ok(())
            },
            _ => {
                Err(Error::from(ErrorKind::Other))
            }
        }
    }

    /// Release all waiters.
    ///
    /// This call broadcasts a semaphore causing all waiters to be
    /// flushed atomically. Any thread which is currently blocked on
    /// the semaphore is unblocked and returns from [get()](Self::get)
    /// or [get_timed()](Self::get_timed) with
    /// [ErrorKind::WouldBlock](https://doc.rust-lang.org/stable/std/io/enum.ErrorKind.html#variant.WouldBlock)
    /// without being granted any resource.
    ///
    /// ```no_run
    /// use revl::sync::semaphore;
    ///
    /// let sem = semaphore::Builder::new().create().unwrap();
    /// sem.flush().unwrap();
    /// ```
    pub fn flush(&self) -> Result<(), Error> {
        let ret: c_int = unsafe {
            evl_flush_sem(self.0.get())
        };
        match ret {
            0 => {
                Ok(())
            },
            _ => {
                Err(Error::from(ErrorKind::Other))
            }
        }
    }
}

impl Drop for Semaphore {
    fn drop(&mut self) {
        unsafe {
            evl_close_sem(self.0.get());
        }
    }
}

impl Default for Semaphore {
    fn default() -> Self {
        Semaphore::new(Builder::default()).unwrap()
    }
}
