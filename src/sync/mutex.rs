//! Interface to EVL mutexes.
//!
//! EVL provides common mutexes for serializing thread access to a
//! shared resource from [out-of-band
//! context](https://evlproject.org/dovetail/pipeline/#two-stage-pipeline),
//! with semantics close to the [POSIX
//! specification](https://pubs.opengroup.org/onlinepubs/9699919799/basedefs/pthread.h.html).
//!
//! The Mutex struct is the Rust interface to the EVL [`mutex`]
//! serialization object.  The implementation borrows from
//! [freertos.rs](https://github.com/hashmismatch/freertos.rs),
//! adapted to the libevl call interface.
//!
//! [`mutex`]: https://evlproject.org/core/user-api/mutex/

use std::ffi::CString;
use std::cell::UnsafeCell;
use std::io::{Error, ErrorKind};
use std::mem::{forget, MaybeUninit};
use std::ops::{Deref, DerefMut};
use std::os::raw::c_int;
use std::fmt;
use std::ptr;
use evl_sys::{
    evl_close_mutex,
    evl_create_mutex,
    evl_lock_mutex,
    evl_trylock_mutex,
    evl_mutex,
    evl_unlock_mutex,
    BuiltinClock,
    CloneFlags,
    MutexType,
};
use crate::clock::CoreClock;

/// A builder contains properties and settings to be used for creating
/// a new mutex.
#[derive(Default, Debug)]
pub struct Builder {
    /// The mutex name. See [this
    /// document](https://evlproject.org/core/user-api/#element-naming-convention)
    /// for details about the naming convention.
    name: Option<String>,
    /// The core clock the timed services should use.
    clock: Option<CoreClock>,
    /// Whether this mutex is visible in the `/dev/evl` hierarchy.
    visible: bool,
    /// Whether this mutex may be locked recursively.
    recursive: bool,
    /// Whether this mutex undergoes the priority inheritance (=0) or
    /// ceiling (> 0) policy.
    ceiling: u32,
}

impl Builder {
    /// Create a mutex builder.
    ///
    /// A default set of properties is used: anonymous mutex
    /// therefore not visible in the `/dev/evl` hierarchy, using the
    /// [`STEADY_CLOCK`] in timed operations, may not be locked
    /// recursively by the same thread and enforces priority
    /// inheritance.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::sync::mutex;
    ///
    /// // Create a mutex which guards a tuple of integers.
    /// let tuple = (0, 0);
    /// let mutex = mutex::Builder::new()
    ///               .name(&format!("some_mutex-{}", std::process::id()))
    ///               .create(tuple);
    ///
    /// assert!(mutex.is_ok());
    /// ```
    ///
    /// [`STEADY_CLOCK`]: crate::clock::STEADY_CLOCK
    pub fn new() -> Self {
        Self {
            name: None,
            clock: None,
            visible: false,
            recursive: false,
            ceiling: 0,
        }
    }

    /// Set the name property. See [this
    /// document](https://evlproject.org/core/user-api/#element-naming-convention)
    /// for details about the naming convention.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::sync::mutex;
    ///
    /// // Create a named mutex which guards a tuple of integers.
    /// let tuple = (0, 0);
    /// let mutex = mutex::Builder::new()
    ///               .name(&format!("some_mutex-{}", std::process::id()))
    ///               .create(tuple);
    ///
    /// assert!(mutex.is_ok());
    /// ```
    pub fn name(mut self, name: &str) -> Self {
        self.name = Some(name.to_string());
        self
    }

    /// Set the reference [`clock`] the mutex should use. All timeouts
    /// passed to timed calls will be based on this clock.
    ///
    /// [`clock`]: crate::clock::CoreClock
    pub fn clock(mut self, clock: CoreClock) -> Self {
        self.clock = Some(clock);
        self
    }

    /// Set the visibility setting to `public`. See
    /// [this document](https://evlproject.org/core/user-api/#element-visibility)
    /// for details about element visibility.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::sync::mutex;
    ///
    /// // Create a publicly visible mutex which guards a tuple of integers.
    /// let tuple = (0, 0);
    /// let mutex = mutex::Builder::new()
    ///               .name(&format!("some_mutex-{}", std::process::id()))
    ///               .public()
    ///               .create(tuple);
    ///
    /// assert!(mutex.is_ok());
    pub fn public(mut self) -> Self {
        self.visible = true;
        self
    }

    /// Set the visibility setting to `private`. See
    /// [this document](https://evlproject.org/core/user-api/#element-visibility)
    /// for details about element visibility.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::sync::mutex;
    ///
    /// // Create a private mutex which guards a tuple of integers.
    /// let tuple = (0, 0);
    /// let mutex = mutex::Builder::new()
    ///               .private()
    ///               .create(tuple);
    ///
    /// assert!(mutex.is_ok());
    pub fn private(mut self) -> Self {
        self.visible = false;
        self
    }

    /// Allow the mutex to be locked recursively.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::sync::mutex;
    ///
    /// // Create a recursive mutex which guards a tuple of integers.
    /// let tuple = (0, 0);
    /// let mutex = mutex::Builder::new()
    ///               .recursive()
    ///               .create(tuple);
    ///
    /// assert!(mutex.is_ok());
    pub fn recursive(mut self) -> Self {
        self.recursive = true;
        self
    }

    /// Set the mutex ceiling value.
    ///
    /// If `ceiling` is non-zero, the priority ceiling protocol is
    /// enabled for the mutex using the mentioned priority. If zero,
    /// priority inheritance is enabled instead (default).
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::sync::mutex;
    ///
    /// // Create a mutex which guards a tuple of integers, enabling
    /// // ceiling at priority 90.
    /// let tuple = (0, 0);
    /// let mutex = mutex::Builder::new()
    ///               .ceiling(90)
    ///               .create(tuple);
    ///
    /// assert!(mutex.is_ok());
    pub fn ceiling(mut self, ceiling: u32) -> Self {
        self.ceiling = ceiling;
        self
    }

    /// Create a new mutex out of this builder.
    ///
    /// # Errors
    ///
    /// Any error from [Mutex::new()](Mutex::new) may be returned.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::sync::mutex;
    ///
    /// // Create a mutex which guards a tuple of integers.
    /// let tuple = (0, 0);
    /// let mutex = mutex::Builder::new()
    ///               .name(&format!("some_mutex-{}", std::process::id()))
    ///               .public()
    ///               .create(tuple);
    ///
    /// assert!(mutex.is_ok());
    /// ```
    pub fn create<T>(self, data: T) -> Result<Mutex<T>, Error> {
        Mutex::new(data, self)
    }
}

/// The Mutex struct implements a mutal exclusion lock.
pub struct Mutex<T: ?Sized> {
    mutex: CoreMutex,
    data: UnsafeCell<T>,
}

unsafe impl<T: ?Sized + Send> Send for Mutex<T> {}
unsafe impl<T: ?Sized + Send> Sync for Mutex<T> {}

impl<T> Mutex<T> {
    /// Create a new mutex for guarding `data`, using the properties
    /// defined by the [`builder`](crate::mutex::Builder).
    ///
    /// # Errors
    ///
    /// [`ErrorKind::AlreadyExists`] if the generated name is
    /// conflicting with an existing mutex, event, semaphore or flag
    /// group name.
    ///
    /// [`ErrorKind::InvalidInput`] if the generated name is badly
    /// formed, or the overall length of the device element's file
    /// path including the generated name exceeds PATH_MAX.
    ///
    /// [`ErrorKind::OutOfMemory`] if not enough memory was available
    /// for completing the operation. By extension, this error status
    /// is also returned if the per-process limit on the number of
    /// open file descriptors or the system-wide limit on the total
    /// number of open files has been reached.
    ///
    /// [`ErrorKind::Other`] on unexpected error.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::sync::mutex::{self, Mutex};
    ///
    /// let builder = mutex::Builder::new()
    ///               .name(&format!("some_mutex-{}", std::process::id()))
    ///               .public();
    ///
    /// // Create a mutex which guards a tuple of integers.
    /// let tuple = (0, 0);
    /// assert!(Mutex::new(tuple, builder).is_ok());
    /// ```
    pub fn new(data: T, builder: Builder) -> Result<Self, Error> {
        Ok(Self {
            mutex: CoreMutex::new(builder)?,
            data: UnsafeCell::new(data),
        })
    }

    /// Lock the mutex.
    ///
    /// This call returns an RAII guard which guarantees exclusive
    /// read/write access to the inner data until such guard goes out
    /// of scope, releasing the mutex. Alternatively, calling [`drop`]
    /// on the guard releases the mutex too.
    ///
    /// # Errors
    ///
    /// [`ErrorKind::WouldBlock`] is returned if the request would
    /// cause the mutex to be locked more than u32::MAX times.
    ///
    /// [`ErrorKind::PermissionDenied`] if the calling thread is not
    /// attached to the EVL core.
    ///
    /// [`ErrorKind::Other`] on unexpected error. By extension, a
    /// deadlock situation is reported using this status as well.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use std::sync::Arc;
    /// use std::thread as native_thread;
    /// use embedded_time::{
    ///     duration::*,
    ///     Clock,
    ///     Instant,
    /// };
    /// use revl::clock::*;
    /// use revl::sched::*;
    /// use revl::thread::{self, *};
    /// use revl::sync::mutex;
    ///
    /// // Create a mutex which guards a floating point value.
    /// let fp = 0.0_f64;
    /// let mutex = Arc::new(mutex::Builder::new()
    ///               .name(&format!("some_mutex-{}", std::process::id()))
    ///               .public()
    ///               .create(fp).unwrap());
    ///
    /// // Create a couple of threads modifying the value concurrently.
    /// for n in 1..=2 {
    ///    let lock = mutex.clone();
    ///
    ///    native_thread::spawn(move || {
    ///         let builder = thread::Builder::new()
    ///            .name(&format!("t{}-{}", n, std::process::id()))
    ///            .sched(SchedAttrs::FIFO(1.into()));
    ///         // Attach the spawned thread to the EVL core.
    ///         Thread::attach(builder).unwrap();
    ///
    ///         loop {
    ///             let mut guard = lock.lock().unwrap();
    ///             *guard += (42.0 * n as f64);
    ///             lock.unlock(guard); // i.e. drop(guard)
    ///             // Sleep 100 us to keep the kernel breathing.
    ///             let timeout = STEADY_CLOCK.now() + Microseconds(100_u32);
    ///             STEADY_CLOCK.sleep_until(timeout).unwrap();
    ///         }
    ///    });
    ///  }
    /// ```
    pub fn lock(&self) -> Result<MutexGuard<T>, Error> {
        self.mutex.lock()?;
        Ok(MutexGuard {
            __mutex: &self.mutex,
            __data: &self.data,
        })
    }

    /// Try locking the mutex.
    ///
    /// On success, this call returns an RAII guard which guarantees
    /// exclusive read/write access to the inner data until such guard
    /// goes out of scope, releasing the mutex. On contention, the
    /// caller does not block waiting for access, but returns with an
    /// error instead.
    ///
    /// [`ErrorKind::WouldBlock`] is returned if the lock is busy,
    /// owned by another thread on entry to this call.
    ///
    /// [`ErrorKind::PermissionDenied`] if the calling thread is not
    /// attached to the EVL core.
    ///
    /// [`ErrorKind::Other`] on unexpected error. By extension, a
    /// deadlock situation is reported using this status as well.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::sync::mutex;
    ///
    /// // Create a mutex which guards an integer value.
    /// let val = 0_u64;
    /// let mutex = mutex::Builder::new()
    ///               .name(&format!("some_mutex-{}", std::process::id()))
    ///               .public()
    ///               .create(val).unwrap();
    ///
    /// // Trylock has to succeed since nobody else shares this mutex yet.
    /// let mut guard = mutex.try_lock().unwrap();
    /// *guard += 42;
    /// mutex.unlock(guard); // i.e. drop(guard)
    /// ```
    pub fn try_lock(&self) -> Result<MutexGuard<T>, Error> {
        self.mutex.try_lock()?;
        Ok(MutexGuard {
            __mutex: &self.mutex,
            __data: &self.data,
        })
    }

    /// Unlock the mutex.
    ///
    /// Immediately drops the guard, and consequently unlocks the
    /// mutex. This call is a self-documenting equivalent to dropping
    /// the RAII guard. Alternately, the guard will be automatically
    /// dropped when it goes out of scope.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::sync::mutex;
    ///
    /// // Create a mutex which guards a floating point value.
    /// let fp = 0.0_f64;
    /// let mutex = mutex::Builder::new()
    ///               .name(&format!("some_mutex-{}", std::process::id()))
    ///               .public()
    ///               .create(fp).unwrap();
    ///
    /// // Lock-modify-unlock sequence:
    /// let mut guard = mutex.lock().unwrap();
    /// *guard += 42.0;
    /// mutex.unlock(guard); // i.e. drop(guard)
    /// ```
    pub fn unlock(&self, guard: MutexGuard<T>) {
        drop(guard);
    }
    
    /// Consume the mutex, returning the inner data.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::sync::mutex;
    ///
    /// // Create a mutex which guards an integer value.
    /// let mutex = mutex::Builder::new()
    ///               .name(&format!("some_mutex-{}", std::process::id()))
    ///               .public()
    ///               .create(42).unwrap();
    ///
    /// assert_eq!(mutex.into_inner(), 42);
    /// ```
    pub fn into_inner(self) -> T {
        unsafe {
            let (mutex, data) = {
                let Self {
                    ref mutex,
                    ref data,
                } = self;
                (ptr::read(mutex), ptr::read(data))
            };
            forget(self);
            drop(mutex);
            data.into_inner()
        }
    }

    /// Return a mutable reference to the underlying data.
    ///
    /// The mutable borrow statically guarantees the caller exclusive
    /// access to the inner data, therefore no locking needs to take
    /// place.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::sync::mutex;
    ///
    /// #[derive(Debug, PartialEq)]
    /// struct Foo {
    ///    fpval: f64,
    ///    ival: u64,
    /// }
    ///
    /// // Create a mutex which guards a struct Foo instance.
    /// let mut mutex = mutex::Builder::new()
    ///               .private()
    ///               .create(Foo { fpval: 42.0, ival: 5150 }).unwrap();
    /// mutex.get_mut().fpval += 1.0;
    /// mutex.get_mut().ival += 5;
    /// assert_eq!(mutex.into_inner(), Foo { fpval: 43.0, ival: 5155 });
    /// ```
    pub fn get_mut(&mut self) -> &mut T {
        self.data.get_mut()
    }
}

impl<T: Default + ?Sized> Default for Mutex<T> {
    fn default() -> Self {
        Mutex::<T>::new(T::default(), Builder::default()).unwrap()
    }
}

impl<T: ?Sized> fmt::Debug for Mutex<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Mutex address: {:?}", self.mutex)
    }
}

pub struct MutexGuard<'a, T: ?Sized + 'a> {
    __mutex: &'a CoreMutex,
    __data: &'a UnsafeCell<T>,
}

impl<'a, T: ?Sized> MutexGuard<'a, T> {
    pub(crate) fn as_raw_mut(&self) -> &'a mut evl_mutex {
        unsafe { &mut *self.__mutex.0.get() }
    }
}

impl<'mutex, T: ?Sized> Deref for MutexGuard<'mutex, T> {
    type Target = T;

    fn deref(&self) -> &T {
        unsafe { &*self.__data.get() }
    }
}

impl<'mutex, T: ?Sized> DerefMut for MutexGuard<'mutex, T> {
    fn deref_mut(&mut self) -> &mut T {
        unsafe { &mut *self.__data.get() }
    }
}

impl<'a, T: ?Sized> Drop for MutexGuard<'a, T> {
    fn drop(&mut self) {
        self.__mutex.unlock();
    }
}

struct CoreMutex(UnsafeCell<evl_mutex>);

impl Drop for CoreMutex {
    fn drop(&mut self) {
        unsafe {
            evl_close_mutex(self.0.get());
        }
    }
}

impl fmt::Debug for CoreMutex {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", &self.0 as *const _)
    }
}

impl CoreMutex {
    /// Create the EVL mutex.
    fn new(builder: Builder) -> Result<Self, Error> {
        let this = Self(UnsafeCell::new(unsafe {
            MaybeUninit::<evl_mutex>::zeroed().assume_init()
        }));
        let mut c_flags = CloneFlags::PRIVATE.bits() as c_int;
        if builder.visible {
            c_flags = CloneFlags::PUBLIC.bits() as c_int;
        }
        if builder.recursive {
            c_flags |= MutexType::RECURSIVE.bits() as c_int;
        }
        let c_ceiling = builder.ceiling;
        let mut c_clockfd = BuiltinClock::MONOTONIC as i32;
        if let Some(clock) = builder.clock {
            c_clockfd = clock.0 as i32;
        }
        let ret: c_int = unsafe {
            if let Some(name) = builder.name {
                let c_name = CString::new(name).map_err(
                    |_| Error::from(ErrorKind::OutOfMemory)
                )?;
                let c_fmt = CString::new("%s").map_err(
                    |_| Error::from(ErrorKind::OutOfMemory)
                )?;
                evl_create_mutex(
                    this.0.get(),
                    c_clockfd,
                    c_ceiling,
                    c_flags,
                    c_fmt.as_ptr(),
                    c_name.as_ptr())
            } else {
                evl_create_mutex(
                    this.0.get(),
                    c_clockfd,
                    c_ceiling,
                    c_flags,
                    ptr::null())
            }
        };
        match ret {
            0.. => {
                Ok(this)
            },
            e if -e == libc::EEXIST => {
                Err(Error::from(ErrorKind::AlreadyExists))
            },
            e if -e == libc::EINVAL || -e == libc::ENAMETOOLONG => {
                Err(Error::from(ErrorKind::InvalidInput))
            },
            e if -e == libc::ENOMEM || -e == libc::EMFILE || -e == libc::ENFILE => {
                Err(Error::from(ErrorKind::OutOfMemory))
            },
            _ => {
                Err(Error::from(ErrorKind::Other))
            },
        }
    }

    /// Lock the EVL mutex.
    fn lock(&self) -> Result<(), Error> {
        let ret: c_int = unsafe { evl_lock_mutex(self.0.get()) };
        match -ret {
            0 => {
                Ok(())
            },
            libc::EPERM => {
                Err(Error::from(ErrorKind::PermissionDenied))
            },
            libc::EAGAIN => {
                Err(Error::from(ErrorKind::WouldBlock))
            },
            _ => {
                Err(Error::from(ErrorKind::Other))
            },
        }
    }

    /// Attempt to lock the EVL mutex.
    fn try_lock(&self) -> Result<(), Error> {
        let ret: c_int = unsafe { evl_trylock_mutex(self.0.get()) };
        match -ret {
            0 => {
                Ok(())
            },
            libc::EPERM => {
                Err(Error::from(ErrorKind::PermissionDenied))
            },
            libc::EAGAIN => {
                Err(Error::from(ErrorKind::WouldBlock))
            },
            _ => {
                Err(Error::from(ErrorKind::Other))
            },
        }
    }

    /// Unlock the EVL mutex.
    fn unlock(&self) {
        unsafe {
            // RAII protects us against ownership violation, so we may
            // pretend there won't be any error (unless the whole
            // thing is busted, at which point nothing is trustable
            // anyway).
            evl_unlock_mutex(self.0.get());
        };
    }
}
