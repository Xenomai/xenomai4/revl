//! Interface to EVL flag groups.
//!
//! An event flag group is an efficient and lightweight mechanism for
//! synchronizing multiple threads, based on a 32bit value, in which
//! each individual bit represents the state of a particular event
//! defined by the application. Therefore, each group defines 32
//! individual events, from bit #0 to bit #31. Threads can wait for
//! bits to be posted by other threads.
//!
//! The Flags struct is the Rust interface to the EVL [flag
//! group](https://evlproject.org/core/user-api/flags/)
//! synchronization object.

use std::cell::UnsafeCell;
use std::ffi::CString;
use std::io::{
    Error,
    ErrorKind
};
use std::mem::MaybeUninit;
use std::os::raw::{c_int, c_long};
use std::ptr;
use libc::{
    self,
    time_t
};
use embedded_time::{
    duration::{Nanoseconds, Seconds},
    fixed_point::FixedPoint,
    Instant,
};
use evl_sys::{
    evl_flags,
    evl_create_flags,
    evl_close_flags,
    evl_wait_some_flags,
    evl_timedwait_some_flags,
    evl_wait_exact_flags,
    evl_timedwait_exact_flags,
    evl_trywait_some_flags,
    evl_trywait_exact_flags,
    evl_peek_flags,
    evl_post_flags,
    evl_broadcast_flags,
    BuiltinClock,
    CloneFlags,
    timespec,
};
use crate::clock::CoreClock;

/// A builder contains properties and settings to be used for creating
/// a new flag group.
#[derive(Default, Debug)]
pub struct Builder {
    /// The flag group name. See [this
    /// document](https://evlproject.org/core/user-api/#element-naming-convention)
    /// for details about the naming convention.
    name: Option<String>,
    /// The core clock the timed services should use.
    clock: Option<CoreClock>,
    /// Whether this flag group is visible in the `/dev/evl` hierarchy.
    visible: bool,
    /// Whether threads should never block reading the flags.
    nonblock: bool,
    /// The initial value of the bitmask.
    initval: u32,
}

impl Builder {
    /// Create a flag group builder.
    ///
    /// A default set of properties is used: anonymous flag group
    /// therefore not visible in the `/dev/evl` hierarchy, using the
    /// [`STEADY_CLOCK`] in timed operations, blocking on wait calls,
    /// initialized to zero.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::sync::flags;
    ///
    /// let flags = flags::Builder::new()
    ///               .name(&format!("some_flags-{}", std::process::id()))
    ///               .public()
    ///               .create();
    ///
    /// assert!(flags.is_ok());
    /// ```
    ///
    /// [`STEADY_CLOCK`]: crate::clock::STEADY_CLOCK
    pub fn new() -> Self {
        Self {
            name: None,
            clock: None,
            visible: false,
            initval: 0u32,
            nonblock: false,
        }
    }

    /// Set the name property. See [this
    /// document](https://evlproject.org/core/user-api/#element-naming-convention)
    /// for details about the naming convention.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::sync::flags;
    ///
    /// let flags = flags::Builder::new()
    ///               .name(&format!("some_flags-{}", std::process::id()))
    ///               .create();
    ///
    /// assert!(flags.is_ok());
    /// ```
    pub fn name(mut self, name: &str) -> Self {
        self.name = Some(name.to_string());
        self
    }

    /// Set the reference [`clock`] the flag group should use. All
    /// timeouts passed to timed calls will be based on this clock.
    ///
    /// [`clock`]: crate::clock::CoreClock
    pub fn clock(mut self, clock: CoreClock) -> Self {
        self.clock = Some(clock);
        self
    }

    /// Set the visibility setting to `public`. See
    /// [this document](https://evlproject.org/core/user-api/#element-visibility)
    /// for details about element visibility.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::sync::flags;
    ///
    /// let flags = flags::Builder::new()
    ///               .name(&format!("some_flags-{}", std::process::id()))
    ///               .public()
    ///               .create();
    ///
    /// assert!(flags.is_ok());
    /// ```
    pub fn public(mut self) -> Self {
        self.visible = true;
        self
    }

    /// Set the visibility setting to `private`. See
    /// [this document](https://evlproject.org/core/user-api/#element-visibility)
    /// for details about element visibility.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::sync::flags;
    ///
    /// let flags = flags::Builder::new()
    ///               .private()
    ///               .create();
    ///
    /// assert!(flags.is_ok());
    /// ```
    pub fn private(mut self) -> Self {
        self.visible = false;
        self
    }

    /// Set the initial value of the flag group to `initval`.
    pub fn init_value(mut self, initval: u32) -> Self {
        self.initval = initval;
        self
    }

    /// Turn on non-blocking mode.
    ///
    /// Use this property to prevent threads from blocking in wait
    /// calls when no flag is pending. In such an event, those calls
    /// would return with `ErrorKind::WouldBlock`.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::sync::flags;
    ///
    /// let flags = flags::Builder::new()
    ///               .nonblock()
    ///               .create();
    ///
    /// assert!(flags.is_ok());
    /// ```
    pub fn nonblock(mut self) -> Self {
        self.nonblock = true;
        self
    }

    /// Create a new flag group out of this builder.
    ///
    /// # Errors
    ///
    /// Any error from [Flags::new()](Flags::new) may be returned.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::sync::flags;
    ///
    /// let flags = flags::Builder::new()
    ///               .name(&format!("some_flags-{}", std::process::id()))
    ///               .public()
    ///               .create();
    ///
    /// assert!(flags.is_ok());
    /// ```
    pub fn create(self) -> Result<Flags, Error> {
        Flags::new(self)
    }
}

/// The Flags struct implements a thread synchronization mechanism
/// based on an atomically-accessed bitmask.
#[derive(Debug)]
pub struct Flags(UnsafeCell<evl_flags>);

unsafe impl Send for Flags {}
unsafe impl Sync for Flags {}

impl Flags {
    /// Create an EVL event flag group, retrieving the settings from a
    /// [`builder struct`](Builder).
    ///
    /// # Errors
    ///
    /// [`ErrorKind::AlreadyExists`] if the generated name is
    /// conflicting with an existing mutex, event, semaphore or flag
    /// group name.
    ///
    /// [`ErrorKind::InvalidInput`] if the generated name is badly
    /// formed, or the overall length of the device element's file
    /// path including the generated name exceeds PATH_MAX.
    ///
    /// [`ErrorKind::OutOfMemory`] if not enough memory was available
    /// for completing the operation. By extension, this error status
    /// is also returned if the per-process limit on the number of
    /// open file descriptors or the system-wide limit on the total
    /// number of open files has been reached.
    ///
    /// [`ErrorKind::Other`] on unexpected error.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::sync::flags::{self, Flags};
    ///
    /// let builder = flags::Builder::new()
    ///               .name(&format!("some_flags-{}", std::process::id()))
    ///               .public();
    ///
    /// assert!(Flags::new(builder).is_ok());
    /// ```
    pub fn new(builder: Builder) -> Result<Self, Error> {
        let this = Self(UnsafeCell::new(unsafe {
            MaybeUninit::<evl_flags>::zeroed().assume_init()
        }));
        let mut c_flags = CloneFlags::PRIVATE.bits() as c_int;
        if builder.visible {
            c_flags = CloneFlags::PUBLIC.bits() as c_int;
        }
        if builder.nonblock {
            c_flags |= CloneFlags::NONBLOCK.bits() as c_int;
        }
        let c_initval = builder.initval as i32;
        let mut c_clockfd = BuiltinClock::MONOTONIC as i32;
        if let Some(clock) = builder.clock {
            c_clockfd = clock.0 as i32;
        }
        let ret: c_int = unsafe {
            if let Some(name) = builder.name {
                let c_name = CString::new(name).map_err(
                    |_| Error::from(ErrorKind::OutOfMemory)
                )?;
                let c_fmt = CString::new("%s").map_err(
                    |_| Error::from(ErrorKind::OutOfMemory)
                )?;
                evl_create_flags(
                    this.0.get(),
                    c_clockfd,
                    c_initval,
                    c_flags,
                    c_fmt.as_ptr(),
                    c_name.as_ptr(),
                )
            } else {
                evl_create_flags(this.0.get(),
                               c_clockfd,
                               c_initval,
                               c_flags,
                               ptr::null())
            }
        };
        match ret {
            0.. => {
                Ok(this)
            },
            e if -e == libc::EEXIST => {
                Err(Error::from(ErrorKind::AlreadyExists))
            },
            e if -e == libc::EINVAL || -e == libc::ENAMETOOLONG => {
                Err(Error::from(ErrorKind::InvalidInput))
            },
            e if -e == libc::ENOMEM || -e == libc::EMFILE || -e == libc::ENFILE => {
                Err(Error::from(ErrorKind::OutOfMemory))
            },
            _ => {
                Err(Error::from(ErrorKind::Other))
            },
        }
    }

    /// Wait for any flag(s) among a given set to be available from
    /// the flag group (i.e. disjunctive wait). The caller may block
    /// until this happens. Waiters are queued by order of scheduling
    /// priority.
    ///
    /// The received flags are atomically cleared from the group then
    /// returned to the caller on success. If non-blocking mode is
    /// enabled for the flag group, the timeout is ignored.
    ///
    /// # Errors
    ///
    /// [`ErrorKind::Interrupted`] if the call was interrupted by an
    /// in-band signal, or forcibly unblocked by the EVL core.
    ///
    /// [`ErrorKind::WouldBlock`] if non-blocking mode is enabled for
    /// the flag group and none of the requested flag(s) is pending on
    /// entry to the call.
    ///
    /// [`ErrorKind::PermissionDenied`] if the calling thread is not
    /// attached to the EVL core.
    ///
    /// [`ErrorKind::Other`] on unexpected error.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::sync::flags;
    ///
    /// let flags = flags::Builder::new()
    ///               .name(&format!("some_flags-{}", std::process::id()))
    ///               .public()
    ///               .create().unwrap();
    ///
    /// assert!(flags.wait_any(0x1111).is_ok());
    /// ```
    pub fn wait_any(&self, bits: u32) -> Result<u32, Error> {
	let mut mask = MaybeUninit::<i32>::uninit();
        let ret: c_int = unsafe {
            evl_wait_some_flags(self.0.get(), bits as i32, mask.as_mut_ptr())
        };
        match -ret {
            0 => {
                Ok(unsafe { mask.assume_init() } as u32)
            },
            libc::EINTR => {
                Err(Error::from(ErrorKind::Interrupted))
            },
            libc::EAGAIN => {
                Err(Error::from(ErrorKind::WouldBlock))
            },
            libc::EPERM => {
                Err(Error::from(ErrorKind::PermissionDenied))
            },
            _ => {
                Err(Error::from(ErrorKind::Other))
            },
        }
    }

    /// Waits for any flag(s) to be available from the flag
    /// group. This call is a shorthand for wait_any(u32:MAX), meaning
    /// that any flag or combination thereof would satisfy the
    /// request.
    ///
    /// The received flags are atomically cleared from the group then
    /// returned to the caller on success.
    ///
    /// # Errors
    ///
    /// [`ErrorKind::Interrupted`] if the call was interrupted by an
    /// in-band signal, or forcibly unblocked by the EVL core.
    ///
    /// [`ErrorKind::WouldBlock`] if non-blocking mode is enabled for
    /// the flag group and no flag is pending on entry to the call.
    ///
    /// [`ErrorKind::PermissionDenied`] if the calling thread is not
    /// attached to the EVL core.
    ///
    /// [`ErrorKind::Other`] on unexpected error.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::sync::flags;
    ///
    /// let flags = flags::Builder::new()
    ///               .name(&format!("some_flags-{}", std::process::id()))
    ///               .public()
    ///               .create().unwrap();
    ///
    /// assert!(flags.wait().is_ok());
    /// ```
    pub fn wait(&self) -> Result<u32, Error> {
        self.wait_any(u32::MAX)
    }
    
    /// Wait for any flag(s) among a given set to be available from
    /// the flag group (i.e. disjunctive wait), with a time limit on
    /// completion. The caller may block until this happens or the
    /// timeout elapses, whichever comes first. Waiters are queued by
    /// order of scheduling priority.
    ///
    /// The received flags are atomically cleared from the group then
    /// returned to the caller on success. If non-blocking mode is
    /// enabled for the flag group, the timeout is ignored.
    ///
    /// # Errors
    ///
    /// [`ErrorKind::TimedOut`] if the timeout fired, after the amount
    /// of time specified by `timeout`.
    ///
    /// [`ErrorKind::Interrupted`] if the call was interrupted by an
    /// in-band signal, or forcibly unblocked by the EVL core.
    ///
    /// [`ErrorKind::WouldBlock`] if non-blocking mode is enabled for
    /// the flag group and none of the requested flag(s) is pending on
    /// entry to the call.
    ///
    /// [`ErrorKind::PermissionDenied`] if the calling thread is not
    /// attached to the EVL core.
    ///
    /// [`ErrorKind::Other`] on unexpected error.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use std::io::ErrorKind;
    /// use embedded_time::{
    ///     duration::*,
    ///     Clock,
    ///     Instant,
    /// };
    /// use revl::clock::*;
    /// use revl::sync::flags;
    ///
    /// let flags = flags::Builder::new()
    ///               .name(&format!("some_flags-{}", std::process::id()))
    ///               .public()
    ///               .create().unwrap();
    ///
    /// // Wait for at most one sec from now.
    /// let timeout = STEADY_CLOCK.now() + Seconds(1_u32);
    /// // Expect a timeout since nobody could have possibly posted these flags yet.
    /// let error = flags.wait_timed_any(0x1111, timeout).expect_err("timeout was expected");
    /// assert_eq!(error.kind(), ErrorKind::TimedOut);
    /// ```
    pub fn wait_timed_any(
        &self, bits: u32, timeout: Instant::<CoreClock>
    ) -> Result<u32, Error> {
        let dur = timeout.duration_since_epoch();
        let secs: Seconds<u64> = Seconds::try_from(dur).unwrap();
        let nsecs: Nanoseconds<u64> = Nanoseconds::<u64>::try_from(dur).unwrap() % secs;
        let date = timespec {
            tv_sec: secs.integer() as time_t,
            tv_nsec: nsecs.integer() as c_long,
        };
	let mut mask = MaybeUninit::<i32>::uninit();
        let ret: c_int = unsafe {
            evl_timedwait_some_flags(
                self.0.get(), bits as i32, &date, mask.as_mut_ptr()
            )
        };
        match -ret {
            0 => {
                Ok(unsafe { mask.assume_init() } as u32)
            },
            libc::ETIMEDOUT => {
                Err(Error::from(ErrorKind::TimedOut))
            },
            libc::EINTR => {
                Err(Error::from(ErrorKind::Interrupted))
            },
            libc::EAGAIN => {
                Err(Error::from(ErrorKind::WouldBlock))
            },
            libc::EPERM => {
                Err(Error::from(ErrorKind::PermissionDenied))
            },
            _ => {
                Err(Error::from(ErrorKind::Other))
            },
        }
    }

    /// Wait for an exact set of flags to be available from the flag
    /// group (i.e. conjunctive wait). The caller may block until this
    /// happens. Waiters are queued by order of scheduling priority.
    ///
    /// The received flags are atomically cleared from the group on
    /// success.
    ///
    /// # Errors
    ///
    /// [`ErrorKind::Interrupted`] if the call was interrupted by an
    /// in-band signal, or forcibly unblocked by the EVL core.
    ///
    /// [`ErrorKind::WouldBlock`] if non-blocking mode is enabled for
    /// the flag group and the request cannot be immediately satisfied
    /// on entry to the call.
    ///
    /// [`ErrorKind::PermissionDenied`] if the calling thread is not
    /// attached to the EVL core.
    ///
    /// [`ErrorKind::Other`] on unexpected error.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::sync::flags;
    ///
    /// let flags = flags::Builder::new()
    ///               .name(&format!("some_flags-{}", std::process::id()))
    ///               .public()
    ///               .create().unwrap();
    ///
    /// assert!(flags.wait_all(0x1111).is_ok());
    /// ```
    pub fn wait_all(&self, bits: u32) -> Result<(), Error> {
        let ret: c_int = unsafe {
            evl_wait_exact_flags(self.0.get(), bits as i32)
        };
        match -ret {
            0 => {
                Ok(())
            },
            libc::EINTR => {
                Err(Error::from(ErrorKind::Interrupted))
            },
            libc::EAGAIN => {
                Err(Error::from(ErrorKind::WouldBlock))
            },
            libc::EPERM => {
                Err(Error::from(ErrorKind::PermissionDenied))
            },
            _ => {
                Err(Error::from(ErrorKind::Other))
            },
        }
    }

    /// Wait for an exact set of flags to be available from the flag
    /// group (i.e. conjunctive wait), with a time limit on
    /// completion. The caller may block until this happens or the
    /// timeout elapses, whichever comes first. Waiters are queued by
    /// order of scheduling priority.
    ///
    /// The received flags are atomically cleared from the group on
    /// success. If non-blocking mode is enabled for the flag group,
    /// the timeout is ignored.
    ///
    /// # Errors
    ///
    /// [`ErrorKind::Interrupted`] if the call was interrupted by an
    /// in-band signal, or forcibly unblocked by the EVL core.
    ///
    /// [`ErrorKind::WouldBlock`] if non-blocking mode is enabled for
    /// the flag group and the request cannot be immediately satisfied
    /// on entry to the call.
    ///
    /// [`ErrorKind::PermissionDenied`] if the calling thread is not
    /// attached to the EVL core.
    ///
    /// [`ErrorKind::Other`] on unexpected error.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use std::io::ErrorKind;
    /// use embedded_time::{
    ///     duration::*,
    ///     Clock,
    ///     Instant,
    /// };
    /// use revl::clock::*;
    /// use revl::sync::flags;
    ///
    /// let flags = flags::Builder::new()
    ///               .name(&format!("some_flags-{}", std::process::id()))
    ///               .public()
    ///               .create().unwrap();
    ///
    /// // Wait for at most one sec from now.
    /// let timeout = STEADY_CLOCK.now() + Seconds(1_u32);
    /// // Expect a timeout since nobody could have possibly posted these flags yet.
    /// let error = flags.wait_timed_all(0x1111, timeout).expect_err("timeout was expected");
    /// assert_eq!(error.kind(), ErrorKind::TimedOut);
    /// ```
    pub fn wait_timed_all(
        &self, bits: u32, timeout: Instant::<CoreClock>
    ) -> Result<(), Error> {
        let dur = timeout.duration_since_epoch();
        let secs: Seconds<u64> = Seconds::try_from(dur).unwrap();
        let nsecs: Nanoseconds<u64> = Nanoseconds::<u64>::try_from(dur).unwrap() % secs;
        let date = timespec {
            tv_sec: secs.integer() as time_t,
            tv_nsec: nsecs.integer() as c_long,
        };
        let ret: c_int = unsafe {
            evl_timedwait_exact_flags(self.0.get(), bits as i32, &date)
        };
        match -ret {
            0 => {
                Ok(())
            },
            libc::EINTR => {
                Err(Error::from(ErrorKind::Interrupted))
            },
            libc::EAGAIN => {
                Err(Error::from(ErrorKind::WouldBlock))
            },
            libc::EPERM => {
                Err(Error::from(ErrorKind::PermissionDenied))
            },
            _ => {
                Err(Error::from(ErrorKind::Other))
            },
        }
    }

    /// Attempt to receive any flag(s) among a given set readily
    /// available from the flag group (i.e. disjunctive wait), without
    /// blocking the caller if there is none.
    ///
    /// The received flags are atomically cleared from the group if
    /// any, and returned to the caller. None otherwise.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::sync::flags;
    ///
    /// let flags = flags::Builder::new()
    ///               .name(&format!("some_flags-{}", std::process::id()))
    ///               .public()
    ///               .create().unwrap();
    ///
    /// // Nobody could have possibly posted these flags yet.
    /// assert!(flags.try_wait_any(0x1111).is_none());
    /// ```
    pub fn try_wait_any(&self, bits: u32) -> Option<u32> {
	let mut mask = MaybeUninit::<i32>::uninit();
        let ret: c_int = unsafe {
            evl_trywait_some_flags(self.0.get(), bits as i32, mask.as_mut_ptr())
        };
        match ret {
            0 => {
                Some(unsafe { mask.assume_init() } as u32)
            },
            _ => {
                None
            },
        }
    }

    /// Attempt to receive any flag(s) currently available from the
    /// flag group.  This call is a shorthand for
    /// try_wait_any(u32:MAX), meaning that any flag or combination
    /// thereof would satisfy the request.
    ///
    /// The received flags are atomically cleared from the group if
    /// any, and returned to the caller. None otherwise.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::sync::flags;
    ///
    /// let flags = flags::Builder::new()
    ///               .name(&format!("some_flags-{}", std::process::id()))
    ///               .public()
    ///               .create().unwrap();
    ///
    /// // Nobody could have possibly posted these flags yet.
    /// assert!(flags.try_wait().is_none());
    /// ```
    pub fn try_wait(&self) -> Option<u32> {
        self.try_wait_any(u32::MAX)
    }
    
    /// Attempt to receive an exact set of flags readily available
    /// from the flag group (i.e. conjunctive wait), without blocking
    /// the caller if the request cannot be satisfied immediately.
    ///
    /// The received flags are atomically cleared from the group if
    /// any, Some(()) is returned. None otherwise.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::sync::flags;
    ///
    /// let flags = flags::Builder::new()
    ///               .name(&format!("some_flags-{}", std::process::id()))
    ///               .public()
    ///               .create().unwrap();
    ///
    /// // Nobody could have possibly posted these flags yet.
    /// assert!(flags.try_wait_exact(0x1111).is_none());
    /// ```
    pub fn try_wait_exact(&self, bits: u32) -> Option<()> {
        let ret: c_int = unsafe {
            evl_trywait_exact_flags(self.0.get(), bits as i32)
        };
        match ret {
            0 => {
                Some(())
            },
            _ => {
                None
            },
        }
    }

    /// Read the current value of a flag group.
    ///
    /// Returns the value of the flag group without blocking or
    /// altering its state (i.e. the flag group is not zeroed if some
    /// events are pending).
    ///
    /// Receiving None from this call should be considered an
    /// unexpected error.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::sync::flags;
    ///
    /// let flags = flags::Builder::new()
    ///               .name(&format!("some_flags-{}", std::process::id()))
    ///               .public()
    ///               .create().unwrap();
    ///
    /// // Nobody could have possibly posted these flags yet.
    /// assert!(flags.peek().is_none());
    /// ```
    pub fn peek(&self) -> Option<u32> {
	let mut mask = MaybeUninit::<i32>::uninit();
        let ret: c_int = unsafe {
            evl_peek_flags(self.0.get(), mask.as_mut_ptr())
        };
        match ret {
            0 => {
                Some(unsafe { mask.assume_init() } as u32)
            },
            _ => {
                None
            },
        }
    }

    /// Post a non-empty set of events to the flag group. A single
    /// waiter whose request is satisfied by the posted flags is
    /// unblocked if any. Waiters are considered for delivery by order
    /// of scheduling priority. Any undelivered bit is left pending in
    /// the group's value.
    ///
    /// # Errors
    ///
    /// [`ErrorKind::InvalidInput`] if `bits` is zero.
    ///
    /// [`ErrorKind::Other`] on unexpected error.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::sync::flags;
    ///
    /// let flags = flags::Builder::new()
    ///               .name(&format!("some_flags-{}", std::process::id()))
    ///               .public()
    ///               .create().unwrap();
    ///
    /// assert!(flags.post(0x1111).is_ok());
    /// ```
    pub fn post(&self, bits: u32) -> Result<(), Error> {
        let c_bits = bits as i32;
        let ret: c_int = unsafe { evl_post_flags(self.0.get(), c_bits) };
        match -ret {
            0 => {
                Ok(())
            },
            libc::EINVAL => {
                Err(Error::from(ErrorKind::InvalidInput))
            },
            _ => {
                Err(Error::from(ErrorKind::Other))
            }
        }
    }

    /// Broadcast a non-empty set of events to the flag group.  Every
    /// waiter which gets its request satisfied by the broadcast flags
    /// is unblocked. In other words, broadcasting means posting a
    /// copy of a set of bits individually to each thread found
    /// waiting on the receiving group at the time of the call. Any
    /// undelivered bit is left pending in the group's value.
    ///
    /// # Errors
    ///
    /// [`ErrorKind::InvalidInput`] if `bits` is zero.
    ///
    /// [`ErrorKind::Other`] on unexpected error.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::sync::flags;
    ///
    /// let flags = flags::Builder::new()
    ///               .name(&format!("some_flags-{}", std::process::id()))
    ///               .public()
    ///               .create().unwrap();
    ///
    /// assert!(flags.broadcast(0x1111).is_ok());
    /// ```
    pub fn broadcast(&self, bits: u32) -> Result<(), Error> {
        let c_bits = bits as i32;
        let ret: c_int = unsafe {
            evl_broadcast_flags(self.0.get(), c_bits)
        };
        match -ret {
            0 => {
                Ok(())
            },
            libc::EINVAL => {
                Err(Error::from(ErrorKind::InvalidInput))
            },
            _ => {
                Err(Error::from(ErrorKind::Other))
            }
        }
    }
}

impl Drop for Flags {
    fn drop(&mut self) {
        unsafe {
            evl_close_flags(self.0.get());
        }
    }
}

impl Default for Flags {
    fn default() -> Self {
        Flags::new(Builder::default()).unwrap()
    }
}
