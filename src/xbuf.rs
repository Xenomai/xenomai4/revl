//! Interface to EVL cross-buffers.
//!
//! A most typical requirement in dual kernel systems is to exchange
//! data between threads running in separate execution stages,
//! i.e. out-of-band vs in-band, without incuring any delay for the
//! [`out-of-band side`].
//!
//! The EVL core implements a feature called a cross-buffer (aka
//! _xbuf_), which connects the sending out-of-band end of a
//! communication channel to its receiving in-band end, and
//! conversely.
//!
//! As a result, data sent via the real-time capable `Write` trait
//! implementation of a cross-buffer can be received by the regular
//! `Read` trait implementation of std::io interfaces. Conversely,
//! data sent via the regular `Write` trait implementation of any
//! std::io interface can be received by the real-time capable `Read`
//! trait implementation of a cross-buffer. Either way, the EVL thread
//! running out-of-band is never downgraded to the in-band stage as a
//! result of performing I/O operations of its side of a cross-buffer.
//!
//! A cross-buffer can be used for transferring fixed-size or
//! variable-size data, supports message-based and byte-oriented
//! streams. Typically, such a mechanism would enable a GUI front-end
//! to receive monitoring data from a real-time work loop.
//!
//! [`out-of-band side`]: https://evlproject.org/dovetail/altsched/

use std::ffi::CString;
use std::ptr;
use std::io::{Error, ErrorKind, Write, Read};
use std::os::raw::{c_int, c_void};
use evl_sys::{
    evl_create_xbuf,
    oob_read,
    oob_write,
    CloneFlags,
};

/// The default size in bytes of the input buffer where the relayed
/// data is kept. bufsz must not exceed 2^30.
pub const DEFAULT_INPUT_BUFSZ: usize = 4096;

/// The default size in bytes of the output buffer where the relayed
/// data is kept. bufsz must not exceed 2^30.
pub const DEFAULT_OUTPUT_BUFSZ: usize = 4096;

/// A cross-buffer builder contains properties and settings to be used
/// for creating a new cross-buffer.
#[derive(Default, Debug)]
pub struct Builder {
    /// The cross-buffer name. See [this
    /// document](https://evlproject.org/core/user-api/#element-naming-convention)
    /// for details about the naming convention.
    name: Option<String>,
    /// The input buffer size.
    i_bufsz: usize,
    /// The output buffer size.
    o_bufsz: usize,
    /// Whether EVL threads should never block during I/O operations
    /// from the out-of-band stage.
    nonblock: bool,
}

impl Builder {
    /// Create a cross-buffer builder.
    ///
    /// A default set of properties is used: anonymous cross-buffer,
    /// therefore not visible in the `/dev/evl` hierarchy, up to
    /// DEFAULT_INPUT_BUFSZ and DEFAULT_OUTPUT_BUFSZ bytes available
    /// for bufferization in I/O respectively, blocking I/O mode on
    /// the real-time side.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::xbuf;
    ///
    /// // Create a cross-buffer.
    /// let xbuf = xbuf::Builder::new()
    ///               .name(&format!("some_xbuf-{}", std::process::id()))
    ///               .create();
    ///
    /// assert!(xbuf.is_ok());
    /// ```
    pub fn new() -> Self {
        Self {
            name: None,
            i_bufsz: DEFAULT_INPUT_BUFSZ,
            o_bufsz: DEFAULT_OUTPUT_BUFSZ,
            nonblock: false,
        }
    }

    /// Set the name property. See [this
    /// document](https://evlproject.org/core/user-api/#element-naming-convention)
    /// for details about the naming convention.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::xbuf;
    ///
    /// // Create a cross-buffer.
    /// let xbuf = xbuf::Builder::new()
    ///               .name(&format!("some_xbuf-{}", std::process::id()))
    ///               .create();
    ///
    /// assert!(xbuf.is_ok());
    /// ```
    pub fn name(mut self, name: &str) -> Self {
        self.name = Some(name.to_string());
        self
    }

    /// Set the input buffer size.
    ///
    /// The input size is the amount of memory available to store data
    /// to be read from the real-time side of the cross-buffer, sent
    /// from the non real-time side.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::xbuf;
    ///
    /// // Create a cross-buffer with a 16k input buffer.
    /// let xbuf = xbuf::Builder::new()
    ///               .name(&format!("some_xbuf-{}", std::process::id()))
    ///               .input_size(16384)
    ///               .create();
    ///
    /// assert!(xbuf.is_ok());
    /// ```
    pub fn input_size(mut self, size: usize) -> Self {
        self.i_bufsz = size;
        self
    }

    /// Set the output buffer size.
    ///
    /// The output size is the amount of memory available to store
    /// data sent from the real-time side of the cross-buffer, to be
    /// read from the non real-time side.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::xbuf;
    ///
    /// // Create a cross-buffer with a 32k output buffer.
    /// let xbuf = xbuf::Builder::new()
    ///               .name(&format!("some_xbuf-{}", std::process::id()))
    ///               .input_size(32768)
    ///               .create();
    ///
    /// assert!(xbuf.is_ok());
    /// ```
    pub fn output_size(mut self, size: usize) -> Self {
        self.o_bufsz = size;
        self
    }

    /// Create a new cross-buffer out of this builder.
    ///
    /// # Errors
    ///
    /// Any error from [XBuf::new()](XBuf::new) may be returned.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::xbuf;
    ///
    /// // Create a cross-buffer.
    /// let xbuf = xbuf::Builder::new()
    ///               .name(&format!("some_xbuf-{}", std::process::id()))
    ///               .create();
    ///
    /// assert!(xbuf.is_ok());
    /// ```
    pub fn create(self) -> Result<XBuf, Error> {
        XBuf::new(self)
    }
}

#[derive(Default, PartialEq, Debug)]
pub struct XBuf(pub(crate) c_int);

unsafe impl Send for XBuf {}
unsafe impl Sync for XBuf {}

impl XBuf {
    /// Create an EVL cross-buffer, retrieving the settings from a
    /// [`builder struct`](Builder).
    ///
    /// The [`Builder`] struct contains the EVL-specific properties to
    /// use.
    ///
    /// # Errors
    ///
    /// [`ErrorKind::AlreadyExists`] if the generated name is
    /// conflicting with an existing cross-buffer name.
    ///
    /// [`ErrorKind::InvalidInput`] if the generated name is badly
    /// formed, or the overall length of the device element's file
    /// path including the generated name exceeds PATH_MAX.
    ///
    /// [`ErrorKind::OutOfMemory`] if not enough memory was available
    /// for completing the operation. By extension, this error status
    /// is also returned if the per-process limit on the number of
    /// open file descriptors or the system-wide limit on the total
    /// number of open files has been reached.
    ///
    /// [`ErrorKind::Other`] on unexpected error.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::xbuf::{self, XBuf};
    ///
    /// // Create a cross-buffer.
    /// let builder = xbuf::Builder::new()
    ///               .name(&format!("some_xbuf-{}", std::process::id()));
    ///
    /// assert!(XBuf::new(builder).is_ok());
    /// ```
    pub fn new(builder: Builder) -> Result<Self, Error> {
        let c_ibufsz = builder.i_bufsz;
        let c_obufsz = builder.o_bufsz;
	let mut c_flags = 0;
        if builder.nonblock {
            c_flags |= CloneFlags::NONBLOCK.bits() as c_int;
        }
	let xfd = unsafe {
            if let Some(name) = builder.name {
	        let c_name = CString::new(name).map_err(
                    |_| Error::from(ErrorKind::OutOfMemory)
                )?;
	        let c_fmt = CString::new("%s").map_err(
                    |_| Error::from(ErrorKind::OutOfMemory)
                )?;
	        evl_create_xbuf(
                    c_ibufsz, c_obufsz, c_flags, c_fmt.as_ptr(), c_name.as_ptr()
                )
            } else {
	        evl_create_xbuf(
                    c_ibufsz, c_obufsz, c_flags, ptr::null()
                )
            }
	};
	// evl_create_xbuf() returns a valid file descriptor or -errno.
	match xfd {
            0.. => {
                Ok(XBuf(xfd))
            },
            e if -e == libc::EEXIST => {
                Err(Error::from(ErrorKind::AlreadyExists))
            },
            e if -e == libc::EINVAL || -e == libc::ENAMETOOLONG => {
                Err(Error::from(ErrorKind::InvalidInput))
            },
            e if -e == libc::ENOMEM || -e == libc::EMFILE || -e == libc::ENFILE => {
                Err(Error::from(ErrorKind::OutOfMemory))
            },
            _ => {
                Err(Error::from(ErrorKind::Other))
            },
	}
    }
}

impl Drop for XBuf {
    fn drop(&mut self) {
        unsafe {
            libc::close(self.0);
        }
    }
}

impl Write for XBuf {
    /// Write to the real-time end of a cross-buffer.
    ///
    /// Sends the content of `buf` to the cross-buffer, without
    /// incurring any transition to the in-band stage. The count of
    /// bytes written through the cross-buffer is returned on success.
    ///
    /// # Errors
    ///
    /// [`ErrorKind::InvalidInput`] if the slice is larger than the
    /// size of the buffer associated to the outbound traffic. For
    /// instance, you could not write more than 512 bytes if
    /// [`output_size()`](Builder::output_size) was set to that limit.
    ///
    /// [`ErrorKind::Unsupported`] if there is no ring buffer space
    /// associated with the outbound traffic of the cross-buffer,
    /// i.e. [`output_size()`](Builder::output_size) was set to zero.
    ///
    /// [`ErrorKind::Other`] on unexpected error.
    fn write(&mut self, buf: &[u8]) -> Result<usize, Error> {
        let data = buf.as_ptr() as *const c_void;
        let ret = unsafe { oob_write(self.0, data, buf.len()) };
        match ret {
            count if count >= 0 => {
                Ok(count as usize)
            },
            e if -e == libc::EINVAL as isize => {
                Err(Error::from(ErrorKind::InvalidInput))
            },
            e if -e == libc::ENOBUFS as isize => {
                Err(Error::from(ErrorKind::Unsupported))
            },
            _ => {
                Err(Error::from(ErrorKind::Other))
            }
        }
    }

    /// Flush the output buffered on the real-time end of a
    /// cross-buffer.
    ///
    /// This call has no effect since the implementation uses
    /// unbuffered I/O in user-space.
    fn flush(&mut self) -> Result<(), Error> {
        Ok(())
    }
}

impl Read for XBuf {
    /// Read from the real-time end of a cross-buffer.
    ///
    /// Reads the content of the cross-buffer into `buf` without
    /// incurring any transition to the in-band stage. The count of
    /// bytes read through the cross-buffer is returned on success.
    ///
    /// # Errors
    ///
    /// [`ErrorKind::InvalidInput`] if the slice is larger than the
    /// size of the buffer associated to the inbound traffic. For
    /// instance, you could not read more than 512 bytes if
    /// [`input_size()`](Builder::input_size) was set to that limit.
    ///
    /// [`ErrorKind::Unsupported`] if there is no ring buffer space
    /// associated with the inbound traffic of the cross-buffer,
    /// i.e. [`input_size()`](Builder::input_size) was set to zero.
    ///
    /// [`ErrorKind::PermissionDenied`] if the calling thread is not
    /// attached to the EVL core.
    ///
    /// [`ErrorKind::Other`] on unexpected error.
    fn read(&mut self, buf: &mut [u8]) -> Result<usize, Error> {
        let data = buf.as_mut_ptr() as *mut c_void;
        let ret = unsafe { oob_read(self.0, data, buf.len()) };
        match ret {
            count if count >= 0 => {
                Ok(count as usize)
            },
            e if -e == libc::EINVAL as isize => {
                Err(Error::from(ErrorKind::InvalidInput))
            },
            e if -e == libc::ENOBUFS as isize => {
                Err(Error::from(ErrorKind::Unsupported))
            },
            e if -e == libc::EPERM as isize => {
                Err(Error::from(ErrorKind::PermissionDenied))
            },
            _ => {
                Err(Error::from(ErrorKind::Other))
            }
        }
    }
}
