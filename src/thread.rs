//! Interface to EVL threads.
//!
//! The thread is the basic execution unit in EVL. An EVL thread is a
//! regular POSIX thread which has attached itself to the EVL
//! core. Once attached, a thread can:
//! 
//! - request EVL real-time services to the core.  In this case, and
//!   only in this one, you get real-time guarantees for the
//!   caller. This is what time-critical processing loops are expected
//!   to use. Such request may switch the calling thread to the
//!   so-called *out-of-band* execution stage for running under EVL's
//!   supervision in order to ensure real-time behaviour.
//! 
//! - invoke services from the main kernel, in which case EVL would
//!   have to demote the caller automatically from the out-of-band to
//!   the in-band stage, so that it enters a runtime mode which is
//!   compatible with using the main kernel services. As a result of
//!   this, the thread would loose all guarantees about short and
//!   bounded latency, in exchange for having access to the features
//!   the main kernel provides. This mode is normally reserved to
//!   initialization and cleanup steps of your application. A common
//!   caveat is to *NOT* use any service from std::alloc when
//!   real-time guarantees are required since this crate may issue
//!   system calls to the main kernel under the hood.
//! 
//! A thread which is being scheduled by EVL instead of the main
//! kernel is said to be running *out-of-band*, as defined by the
//! [`Dovetail`] kernel layer. Such thread remains in this mode until
//! it asks for a service which the main kernel provides.  Conversely,
//! a thread which is being scheduled by the main kernel instead of
//! EVL is said to be running *in-band*. It remains in this mode until
//! it asks for a service which EVL can only provide to the caller
//! when running out-of-band.
//!
//! The Thread struct is the Rust interface to the EVL [`thread`]
//! support.
//!
//! [`thread`]: https://evlproject.org/core/user-api/thread/
//! [`Dovetail`]: https://evlproject.org/dovetail/pipeline/

use core::mem::MaybeUninit;
use std::ptr;
use std::os::raw::c_int;
use std::io::{Error, ErrorKind};
use std::ffi::CString;
use evl_sys::{
    evl_attach_thread,
    evl_detach_self,
    evl_get_self,
    evl_is_inband,
    evl_switch_inband,
    evl_switch_oob,
    evl_unblock_thread,
    evl_demote_thread,
    evl_sched_attrs,
    evl_set_schedattr,
    evl_get_schedattr,
    CloneFlags,
};
use crate::sched::*;

/// A builder contains properties and settings to be used for creating
/// a new thread.
#[derive(Default)]
pub struct Builder {
    /// The thread name. See [this
    /// document](https://evlproject.org/core/user-api/#element-naming-convention)
    /// for details about the naming convention.
    name: Option<String>,
    /// Whether this thread is visible in the `/dev/evl` hierarchy.
    visible: bool,
    /// Whether this thread is [observable](https://evlproject.org/core/user-api/observable/).
    observable: bool,
    /// Whether notifications should be sent to a single/all observers.
    unicast: bool,
    /// Whether observers read requests should be non-blocking.
    nonblock: bool,
    /// The scheduling attributes of the thread.
    sched: Option<evl_sched_attrs>,
}

impl Builder {
    /// Create a thread builder.
    ///
    /// A default set of properties is used: anonymous thread,
    /// therefore not visible in the `/dev/evl` hierarchy, not
    /// observable, inherits the scheduling parameters of the thread
    /// calling [attach()](Self::attach). See the translation rules
    /// between POSIX and [EVL scheduling
    /// classes](https://evlproject.org/core/user-api/scheduling/#sched-services).
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::thread::*;
    ///
    /// let thread = Builder::new()
    ///               .name(&format!("some_thread-{}", std::process::id()))
    ///               .public()
    ///               .attach();
    ///
    /// assert!(thread.is_ok());
    /// ```
    pub fn new() -> Self {
        Self {
            name: None,
            visible: false,
            observable: false,
            unicast: false,
            sched: None,
            nonblock: false,
        }
    }

    /// Set the name property. See [this
    /// document](https://evlproject.org/core/user-api/#element-naming-convention)
    /// for details about the naming convention.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::thread::*;
    ///
    /// // Attach a named thread.
    /// let thread = Builder::new()
    ///               .name(&format!("some_thread-{}", std::process::id()))
    ///               .public()
    ///               .attach();
    ///
    /// assert!(thread.is_ok());
    /// ```
    pub fn name(mut self, name: &str) -> Self {
        self.name = Some(name.to_string());
        self
    }

    /// Set the visibility setting to `public`. See
    /// [this document](https://evlproject.org/core/user-api/#element-visibility)
    /// for details about element visibility.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::thread::*;
    ///
    /// // Attach a publicly visible thread.
    /// let thread = Builder::new()
    ///               .name(&format!("some_thread-{}", std::process::id()))
    ///               .public()
    ///               .attach();
    ///
    /// assert!(thread.is_ok());
    /// ```
    pub fn public(mut self) -> Self {
        self.visible = true;
        self
    }

    /// Set the visibility setting to `private`. See
    /// [this document](https://evlproject.org/core/user-api/#element-visibility)
    /// for details about element visibility.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::thread::*;
    ///
    /// // Attach a private thread.
    /// let thread = Builder::new().attach();
    ///
    /// assert!(thread.is_ok());
    /// ```
    pub fn private(mut self) -> Self {
        self.visible = false;
        self
    }

    /// Make the thread observable for health monitoring purpose, as
    /// defined by this
    /// [document](https://evlproject.org/core/user-api/observable/#observable-thread).
    ///
    /// See [`nonblock()`](Self::nonblock] for non-blocking read requests from observers.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::thread::*;
    ///
    /// // Attach an observable, public thread.
    /// let thread = Builder::new()
    ///               .name(&format!("some_thread-{}", std::process::id()))
    ///               .public()
    ///               .observable()
    ///               .attach();
    ///
    /// assert!(thread.is_ok());
    /// ```
    pub fn observable(mut self) -> Self {
        self.observable = true;
        self
    }

    /// Enable unicast mode for an observable thread, restricting the
    /// notification of events to a single observer.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::thread::*;
    ///
    /// // Attach a observable, private thread with unicast notifications.
    /// let thread = Builder::new()
    ///               .name(&format!("some_thread-{}", std::process::id()))
    ///               .private()
    ///               .observable()
    ///               .unicast()
    ///               .attach();
    ///
    /// assert!(thread.is_ok());
    /// ```
    pub fn unicast(mut self) -> Self {
        self.unicast = true;
        self
    }

    /// Set the scheduling parameters for the thread.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::thread::*;
    /// use revl::sched::*;
    ///
    /// // Attach a thread undergoing the SCHED_FIFO policy at priority 50.
    /// let thread = Builder::new()
    ///               .name(&format!("some_thread-{}", std::process::id()))
    ///               .sched(SchedAttrs::FIFO(50.into()))
    ///               .attach();
    ///
    /// assert!(thread.is_ok());
    /// ```
    pub fn sched(mut self, attrs: SchedAttrs) -> Self {
        self.sched = Some(attrs.into());
        self
    }

    /// Turn on non-blocking mode for observers.
    ///
    /// When a thread is observable, observers might want to try
    /// reading event notifications without blocking if none is
    /// pending. Use this property to enable non-blocking read mode
    /// for any observer of such thread.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::thread::*;
    ///
    /// // Attach a thread with non-blocking read requests for observers.
    /// let thread = Builder::new()
    ///               .name(&format!("some_thread-{}", std::process::id()))
    ///               .nonblock()
    ///               .attach();
    ///
    /// assert!(thread.is_ok());
    /// ```
    pub fn nonblock(mut self) -> Self {
        self.nonblock = true;
        self
    }

    /// Attach the calling thread to the EVL core.
    ///
    /// # Errors
    ///
    /// Any error from [Thread::attach()](Thread::attach) may be returned.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::thread::*;
    /// use revl::sched::*;
    ///
    /// // Attach ourselves as a publicly visible thread undergoing the
    /// // SCHED_FIFO policy at priority 1.
    /// let thread = Builder::new()
    ///               .name(&format!("some_thread-{}", std::process::id()))
    ///               .sched(SchedAttrs::FIFO(1.into()))
    ///               .public()
    ///               .attach();
    ///
    /// assert!(thread.is_ok());
    /// ```
    pub fn attach(self) -> Result<Thread, Error> {
        Thread::attach(self)
    }
}

#[derive(PartialEq, Debug)]
pub struct Thread(pub(crate) c_int);

unsafe impl Send for Thread {}
unsafe impl Sync for Thread {}

impl Thread {
    /// Attach the calling thread to the EVL core.
    ///
    /// The [`Builder`] struct contains the EVL-specific properties to
    /// use. The call returns an EVL thread descriptor mapped to the
    /// current thread.
    ///
    /// Unlike other EVL resources / elements, threads are not created
    /// by EVL. Instead, existing threads may attach themselves to a
    /// running core to gain real-time capabilities.
    ///
    /// An exiting thread is automatically detached by the EVL core,
    /// there is no need for a thread to call
    /// [`detach()`](Self::detach) explicitly before exit. Once an EVL
    /// thread has exited, any request via a lingering thread
    /// descriptor still referring to it yields an error denoting a
    /// stale status.
    ///
    /// A thread descriptor is merely a handle, dropping it has no
    /// effect on the attachment of the matching EVL thread.
    ///
    /// # Errors
    ///
    /// [`ErrorKind::AlreadyExists`] if the generated name is
    /// conflicting with an existing thread name.
    ///
    /// [`ErrorKind::InvalidInput`] if the generated name is badly
    /// formed, or the overall length of the device element's file
    /// path including the generated name exceeds PATH_MAX.
    ///
    /// [`ErrorKind::OutOfMemory`] if not enough memory was available
    /// for completing the operation. By extension, this error status
    /// is also returned if the per-process limit on the number of
    /// open file descriptors or the system-wide limit on the total
    /// number of open files has been reached.
    ///
    /// [`ErrorKind::PermissionDenied`] if the calling thread is not
    /// granted the privileges required by the attachment operation,
    /// such as locking memory via the
    /// [mlockall(2)](http://man7.org/linux/man-pages/man2/mlock.2.html)
    /// system call.
    ///
    /// [`ErrorKind::Other`] on unexpected error. By extension, this
    /// may mean that either the EVL core is not enabled in the
    /// kernel, or there is an ABI mismatch between the underlying
    /// [evl-sys
    /// crate](https://source.denx.de/Xenomai/xenomai4/evl-sys) and
    /// the EVL core. See [these
    /// explanations](https://evlproject.org/core/under-the-hood/abi/)
    /// for the latter.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::thread::*;
    /// use revl::sched::*;
    ///
    /// let builder = Builder::new()
    ///               .name(&format!("some_thread-{}", std::process::id()))
    ///               .sched(SchedAttrs::FIFO(1.into()))
    ///               .public();
    ///
    /// // Attach ourselves as a publicly visible thread undergoing the
    /// // SCHED_FIFO policy at priority 1.
    /// assert!(Thread::attach(builder).is_ok());
    /// ```
    pub fn attach(builder: Builder) -> Result<Self, Error> {
	let mut c_flags = CloneFlags::PRIVATE.bits() as c_int;
        if builder.visible {
	    c_flags = CloneFlags::PUBLIC.bits() as c_int;
        }
        if builder.observable {
	    c_flags |= CloneFlags::OBSERVABLE.bits() as c_int;
        }
        if builder.unicast {
	    c_flags |= CloneFlags::UNICAST.bits() as c_int;
        }
        if builder.nonblock {
	    c_flags |= CloneFlags::NONBLOCK.bits() as c_int;
        }
	let tfd: c_int = unsafe {
            if let Some(name) = builder.name {
	        let c_name = CString::new(name).map_err(
                    |_| Error::from(ErrorKind::OutOfMemory)
                )?;
	        let c_fmt = CString::new("%s").map_err(
                    |_| Error::from(ErrorKind::OutOfMemory)
                )?;
	        evl_attach_thread(c_flags, c_fmt.as_ptr(), c_name.as_ptr())
            } else {
                // Anonymous thread (has to be private, the core will
                // check this).
	        evl_attach_thread(c_flags, ptr::null())
            }
	};
	// evl_attach_thread() returns a valid file descriptor or -errno.
	match tfd {
	    0.. => {
                // Since we are now successfully attached to the core,
                // we may update our runtime sched params EVL-wise.
                if let Some(attrs) = builder.sched {
	            let c_attrs_ptr: *const evl_sched_attrs = &attrs;
	            let ret: c_int = unsafe { evl_set_schedattr(tfd, c_attrs_ptr) };
	            if ret < 0 {
                        return match -ret {
                            libc::EINVAL => {
                                Err(Error::from(ErrorKind::InvalidInput))
                            },
                            _ => {
                                Err(Error::from(ErrorKind::Other))
                            }
                        };
	            }
                }
                Ok(Thread(tfd))
            },
            e if -e == libc::EEXIST => {
                Err(Error::from(ErrorKind::AlreadyExists))
            },
            e if -e == libc::EINVAL || -e == libc::ENAMETOOLONG => {
                Err(Error::from(ErrorKind::InvalidInput))
            },
            e if -e == libc::ENOMEM || -e == libc::EMFILE || -e == libc::ENFILE => {
                Err(Error::from(ErrorKind::OutOfMemory))
            },
            _ => {
                Err(Error::from(ErrorKind::Other))
            },
	}
    }

    /// Detach the calling thread from the EVL core.
    ///
    /// The calling thread is unregistered from the EVL core. This is
    /// the converse operation to [`attach()`](Self::attach).
    ///
    /// # Errors
    ///
    /// [`ErrorKind::PermissionDenied`] if the calling thread is not
    /// attached to the EVL core.
    ///
    /// [`ErrorKind::Other`] on unexpected error.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::thread::*;
    /// use revl::sched::*;
    ///
    /// let builder = Builder::new()
    ///               .name(&format!("some_thread-{}", std::process::id()))
    ///               .sched(SchedAttrs::FIFO(1.into()))
    ///               .public();
    ///
    /// // Attach ourselves as a publicly visible thread undergoing the
    /// // SCHED_FIFO policy at priority 1.
    /// assert!(Thread::attach(builder).is_ok());
    /// // Next we may detach from the core.
    /// assert!(Thread::detach().is_ok());
    /// ```
    pub fn detach() -> Result<(), Error> {
	let ret: c_int = unsafe { evl_detach_self() };
        match -ret {
            0 => {
                Ok(())
            },
            libc::EPERM => {
                Err(Error::from(ErrorKind::PermissionDenied))
            },
            _ => {
                Err(Error::from(ErrorKind::Other))
            },
        }
    }

    /// Retrieve the current thread descriptor.
    ///
    /// If the caller is attached to the EVL core, returns its EVL
    /// thread descriptor, otherwise None.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::thread::*;
    /// use revl::sched::*;
    ///
    /// // Attach ourselves to the EVL core.
    /// let me = Builder::new().attach().unwrap();
    ///
    /// assert_eq!(Thread::current(), Some(me));
    /// ```
    pub fn current() -> Option<Self> {
	let tfd = unsafe { evl_get_self() };
        (tfd >= 0).then(|| Thread(tfd))
    }

    /// Switch the caller to the in-band execution stage.
    ///
    /// An EVL thread running out-of-band may switch in-band, at the
    /// expense of all real-time guarantees. When called from a
    /// regular thread (i.e. not attached to the EVL core), this
    /// operation immediately returns with a success status. As a
    /// consequence, this call should never fail in the current
    /// implementation.
    ///
    /// # Errors
    ///
    /// [`ErrorKind::Other`] on unexpected error.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::thread::*;
    /// use revl::sched::*;
    ///
    /// // Attach ourselves to the EVL core with a real-time priority.
    /// let me = Builder::new()
    ///               .sched(SchedAttrs::FIFO(1.into()))
    ///               .attach().unwrap();
    ///
    /// // EVL threads undergoing a real-time scheduling policy are running
    /// // out-of-band right after attachment.
    /// assert!(Thread::switch_inband().is_ok());
    /// ```
    pub fn switch_inband() -> Result<(), Error> {
	let ret = unsafe { evl_switch_inband() };
        match ret {
            0 => {
                Ok(())
            },
            _ => {
                Err(Error::from(ErrorKind::Other))
            },
        }
    }

    /// Switch the caller to the out-of-band execution stage.
    ///
    /// An EVL thread running in-band may switch out-of-band,
    /// regaining real-time guarantees as a result. Any request from
    /// such thread which already runs out-of-band is silently
    /// ignored, returning with a success status.
    ///
    /// # Errors
    ///
    /// [`ErrorKind::PermissionDenied`] if the calling thread is not
    /// attached to the EVL core.
    ///
    /// [`ErrorKind::Other`] on unexpected error.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::thread::*;
    /// use revl::sched::*;
    ///
    /// // Attach ourselves to the EVL core with a non real-time priority.
    /// let me = Builder::new()
    ///               .sched(SchedAttrs::WEAK(0.into()))
    ///               .attach().unwrap();
    ///
    /// // EVL threads undergoing the weak scheduling policy are running
    /// // in-band right after attachment.
    /// assert!(Thread::switch_oob().is_ok());
    /// ```
    pub fn switch_oob() -> Result<(), Error> {
	let ret = unsafe { evl_switch_oob() };
        match -ret {
            0 => {
                Ok(())
            },
            libc::EPERM => {
                Err(Error::from(ErrorKind::PermissionDenied))
            },
            _ => {
                Err(Error::from(ErrorKind::Other))
            },
        }
    }

    /// Tell whether the caller is running in-band.
    ///
    /// Both EVL threads (i.e. attached to the EVL core) and regular
    /// threads may run in-band, with the latter always doing so.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::thread::*;
    /// use revl::sched::*;
    ///
    /// // Attach ourselves to the EVL core with a real-time priority.
    /// let me = Builder::new()
    ///               .sched(SchedAttrs::FIFO(1.into()))
    ///               .attach().unwrap();
    ///
    /// // EVL threads undergoing a real-time scheduling policy are running
    /// // out-of-band right after attachment.
    /// assert!(!Thread::is_inband());
    /// ```
    pub fn is_inband() -> bool {
	unsafe { evl_is_inband() }
    }

    /// Tell whether the caller is running out-of-band.
    ///
    /// Only EVL threads (i.e. attached to the EVL core) may run
    /// [`out-of-band`].
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::thread::*;
    /// use revl::sched::*;
    ///
    /// // Attach ourselves to the EVL core with a real-time priority.
    /// let me = Builder::new()
    ///               .sched(SchedAttrs::FIFO(1.into()))
    ///               .attach().unwrap();
    ///
    /// // EVL threads undergoing a real-time scheduling policy are running
    /// // out-of-band right after attachment.
    /// assert!(Thread::is_oob());
    /// ```
    ///
    /// [`out-of-band`]: https://evlproject.org/dovetail/altsched/
    pub fn is_oob() -> bool {
	!Self::is_inband()
    }

    /// Unblock `self`.
    ///
    /// If `self is currently sleeping on some EVL core system call,
    /// such call is forced to fail. As a result, `self` wakes up with
    /// an interrupted call status on return.
    ///
    /// # Errors
    ///
    /// [`ErrorKind::BrokenPipe`] if `self` is stale.
    ///
    /// [`ErrorKind::Other`] on unexpected error.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use std::sync::Arc;
    /// use revl::thread::*;
    ///
    /// fn forcible_unblock(t: Arc<Thread>) {
    ///     assert!(t.unblock().is_ok());
    /// }
    /// ```
    pub fn unblock(&self) -> Result<(), Error> {
	let ret: c_int = unsafe { evl_unblock_thread(self.0) };
        match -ret {
            0 => {
                Ok(())
            },
            libc::ESTALE => {
                Err(Error::from(ErrorKind::BrokenPipe))
            },
            _ => {
                Err(Error::from(ErrorKind::Other))
            },
        }
    }

    /// Demote `self` to in-band context.
    ///
    /// Demoting a thread means to force it out of any real-time
    /// scheduling class, unblock it like unblock() would do, and kick
    /// it out of the out-of-band stage, all in the same move.  See
    /// details and caveat
    /// [here](https://evlproject.org/core/user-api/thread/#evl_demote_thread).
    ///
    /// # Errors
    ///
    /// [`ErrorKind::BrokenPipe`] if `self` is stale.
    ///
    /// [`ErrorKind::Other`] on unexpected error.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use std::sync::Arc;
    /// use revl::thread::*;
    ///
    /// fn stage_demotion(t: Arc<Thread>) {
    ///     assert!(t.demote().is_ok());
    /// }
    /// ```
    pub fn demote(&self) -> Result<(), Error> {
	let ret: c_int = unsafe { evl_demote_thread(self.0) };
        match -ret {
            0 => {
                Ok(())
            },
            libc::ESTALE => {
                Err(Error::from(ErrorKind::BrokenPipe))
            },
            _ => {
                Err(Error::from(ErrorKind::Other))
            },
        }
    }

    /// Set the scheduling attributes of `self` to `attrs`.
    ///
    /// # Errors
    ///
    /// [`ErrorKind::InvalidInput`] if `attrs` contains invalid
    /// settings.
    ///
    /// [`ErrorKind::BrokenPipe`] if `self` is stale.
    ///
    /// [`ErrorKind::Other`] on unexpected error.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::thread::*;
    /// use revl::sched::*;
    ///
    /// let builder = Builder::new()
    ///               .name(&format!("some_thread-{}", std::process::id()))
    ///               .sched(SchedAttrs::FIFO(1.into()))
    ///               .public();
    ///
    /// // Attach ourselves as a publicly visible thread undergoing the
    /// // SCHED_FIFO policy at priority 1.
    /// let me = Thread::attach(builder).unwrap();
    /// // Raise our priority by one.
    /// assert!(me.set_sched(SchedAttrs::FIFO(2.into())).is_ok());
    /// ```
    pub fn set_sched(&self, attrs: SchedAttrs) -> Result<(), Error> {
	let c_attrs_ptr: *const evl_sched_attrs = &attrs.into();
	let ret: c_int = unsafe { evl_set_schedattr(self.0, c_attrs_ptr) };
        match -ret {
            0 => {
                Ok(())
            },
            libc::EINVAL => {
                Err(Error::from(ErrorKind::InvalidInput))
            },
            _ => {
                Err(Error::from(ErrorKind::Other))
            }
        }
    }

    /// Get the current scheduling attributes of a thread.
    ///
    /// # Errors
    ///
    /// [`ErrorKind::InvalidInput`] if the current scheduling policy
    /// of `self` is unknown to the Rust interface - which might
    /// happen in case of an ABI mismatch between the evl-sys FFI
    /// bindings its is based on and the EVL core running in kernel
    /// space.
    ///
    /// [`ErrorKind::Other`] on unexpected error.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::thread::*;
    /// use revl::sched::*;
    ///
    /// let builder = Builder::new()
    ///               .name(&format!("some_thread-{}", std::process::id()))
    ///               .sched(SchedAttrs::FIFO(20.into()))
    ///               .public();
    ///
    /// // Attach ourselves as a publicly visible thread undergoing the
    /// // SCHED_FIFO policy at priority 20.
    /// let me = Thread::attach(builder).unwrap();
    ///
    /// // Retrieve then check our scheduling parameters.
    /// let params = me.sched().unwrap();
    /// match params {
    ///    SchedAttrs::FIFO(setting) => {
    ///       assert_eq!(setting.prio, 20);
    ///    }
    ///    _ => {
    ///       panic!("unexpected return from Thread::sched()");
    ///    }
    /// };
    /// ```
    pub fn sched(&self) -> Result<SchedAttrs, Error> {
	let mut attrs = MaybeUninit::<evl_sched_attrs>::uninit();
	let ret: c_int = unsafe { evl_get_schedattr(self.0, attrs.as_mut_ptr()) };
	match ret {
	    0 => {
                let attrs = unsafe { attrs.assume_init().into() };
                match attrs {
                    SchedAttrs::UNKNOWN => {
                        Err(Error::from(ErrorKind::InvalidInput))
                    },
                    _ => {
                        Ok(attrs)
                    }
                }
            },
            _ => {
                Err(Error::from(ErrorKind::Other))
            }
	}
    }
}
