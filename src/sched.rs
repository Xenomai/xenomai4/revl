//! Interface to the out-of-band scheduler.
//! 
//! EVL defines five scheduling policies for running [`out-of-band
//! threads`].  These policies are hierarchized: every time the core
//! needs to pick the next eligible thread to run on the current CPU,
//! it queries each policy module for a runnable thread in the
//! following order:
//! 
//! 1. SCHED_FIFO, which is the common first-in, first-out real-time
//! policy.
//!
//! 2. SCHED_RR which is the round-robin policy, based on the
//! SCHED_FIFO priority model.
//! 
//! 3. SCHED_TP, which enforces temporal partitioning of multiple sets
//! of threads in a way which prevents those sets from overlapping
//! time-wise on the CPU which runs such policy.
//! 
//! 4. SCHED_QUOTA, which enforces a limitation on the CPU consumption
//! of threads over a fixed period of time.
//! 
//! 5. SCHED_WEAK, which is a **non real-time** policy allowing its
//! members to run in-band most of the time, while retaining the
//! ability to request EVL services.
//! 
//! 6. SCHED_IDLE, which is the fallback option the EVL core considers
//! only when other policies have no runnable task on the CPU. This
//! policy is not directly available to the user.
//! 
//! The SCHED_QUOTA and SCHED_TP policies are optionally supported by
//! the core, make sure to enable [`CONFIG_EVL_SCHED_QUOTA`] or
//! [`CONFIG_EVL_SCHED_TP`] respectively in the kernel configuration
//! if you need them.
//! 
//! Before a thread can be assigned to any EVL scheduling class, it
//! must attach itself to the core by a call to
//! [`Thread::attach()`](crate::thread::Thread::attach).
//!
//! [`out-of-band threads`]: https://evlproject.org/dovetail/altsched/
//! [`CONFIG_EVL_SCHED_QUOTA`]: https://evlproject.org/core/user-api/scheduling/#SCHED_QUOTA
//! [`CONFIG_EVL_SCHED_TP`]: https://evlproject.org/core/user-api/scheduling/#SCHED_TP

// Re-export scheduling attributes.
pub use evl_sys::{
    SchedAttrs,
    SchedOther,
    SchedFifo,
    SchedRR,
    SchedWeak,
    SchedQuota,
    SchedTP
};
