//! Interface to EVL proxies.
//!
//! A common issue with dual kernel systems stems from the requirement
//! **not** to issue [`in-band system calls`] while running
//! time-critical code in out-of-band context. Problem is that
//! sometimes, you may need - for instance - to write to regular files
//! such as for logging information to the console or elsewhere from
//! an out-of-band work loop. Doing so by calling common [`stdio(3)`]
//! services indirectly is therefore not an option due to the latency
//! this would incur.
//!
//! The EVL core solves this problem with the *file proxy* feature,
//! which can push data to an arbitrary target file, and/or conversely
//! read data pulled from such target file, providing an [`out-of-band
//! I/O interface`] to the data producer and consumer threads. This
//! works by offloading the I/O transfers to internal worker threads
//! running in-band which relay the data to/from the target file. For
//! this reason, I/O transfers may be delayed until the in-band stage
//! resumes on a worker's CPU.  For instance,
//! [evl_println()](crate::proxy:evl_println) emits the output to the
//! standard output stream of the current process via an internal
//! proxy.
//!
//! The Proxy struct is the Rust interface to EVL's [proxy
//! element](https://evlproject.org/core/user-api/proxy/).
//!
//! [`in-band system calls`]: https://evlproject.org/dovetail/altsched/
//! [`stdio(3)`](http://man7.org/linux/man-pages/man3/stdio.3.html)
//! [`out-of-band I/O interface`] https://evlproject.org/core/user-api/io/

use std::ffi::{CString, c_void};
use std::ptr;
use std::fmt;
use std::sync::Once;
use std::io::{Error, ErrorKind, Write, Read};
use std::os::unix::io::AsRawFd;
use std::os::raw::c_int;
use evl_sys::{
    evl_create_proxy,
    evl_write_proxy,
    evl_read_proxy,
    CloneFlags,
};

/// One-time init flag for proxies channeling stdout/err.
static STD_INIT: Once = Once::new();

/// The size in bytes of the I/O ring buffer where the relayed data is
/// kept. bufsz must not exceed 2^30. A ring is allocated for each
/// enabled I/O direction.
pub const DEFAULT_PROXY_BUFSZ: usize = 4096;

/// A proxy builder contains properties and settings to be used for
/// creating a new proxy.
#[derive(Default, Debug)]
pub struct Builder {
    /// The proxy name. See [this
    /// document](https://evlproject.org/core/user-api/#element-naming-convention)
    /// for details about the naming convention.
    name: Option<String>,
    /// Whether this proxy is visible in the `/dev/evl` hierarchy.
    visible: bool,
    /// The proxy buffer size.
    bufsz: usize,
    /// The I/O size granularity.
    granularity: usize,
    /// Whether this proxy enables the output direction.
    output: bool,
    /// Whether this proxy enables the input direction.
    input: bool,
    /// Whether threads should never block during I/O operations.
    nonblock: bool,
}

impl Builder {
    /// Create a proxy builder.
    ///
    /// A default set of properties is used: anonymous proxy,
    /// therefore not visible in the `/dev/evl` hierarchy, up to
    /// DEFAULT_PROXY_BUFSZ bytes available for bufferization,
    /// unspecified I/O granularity, blocking I/O mode, for output
    /// only.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::proxy;
    ///
    /// // Create a publicly visible proxy to stdout.
    /// let target = std::io::stdout();
    /// let proxy = proxy::Builder::new()
    ///               .name(&format!("some_proxy-{}", std::process::id()))
    ///               .public()
    ///               .create(target);
    ///
    /// assert!(proxy.is_ok());
    /// ```
    pub fn new() -> Self {
        Self {
            name: None,
            bufsz: DEFAULT_PROXY_BUFSZ,
            granularity: 0,     // i.e. Unspecified.
            visible: false,
            output: true,
            input: false,
            nonblock: false,
        }
    }

    /// Set the name property. See [this
    /// document](https://evlproject.org/core/user-api/#element-naming-convention)
    /// for details about the naming convention.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::proxy;
    ///
    /// // Create a named proxy to stdout.
    /// let target = std::io::stdout();
    /// let proxy = proxy::Builder::new()
    ///               .name(&format!("some_proxy-{}", std::process::id()))
    ///               .create(target);
    ///
    /// assert!(proxy.is_ok());
    /// ```
    pub fn name(mut self, name: &str) -> Self {
        self.name = Some(name.to_string());
        self
    }

    /// Set the visibility setting to `public`. See
    /// [this document](https://evlproject.org/core/user-api/#element-visibility)
    /// for details about element visibility.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::proxy;
    ///
    /// // Create a publicly visible proxy to stdout.
    /// let target = std::io::stdout();
    /// let proxy = proxy::Builder::new()
    ///               .name(&format!("some_proxy-{}", std::process::id()))
    ///               .public()
    ///               .create(target);
    ///
    /// assert!(proxy.is_ok());
    /// ```
    pub fn public(mut self) -> Self {
        self.visible = true;
        self
    }

    /// Set the visibility setting to `private`. See
    /// [this document](https://evlproject.org/core/user-api/#element-visibility)
    /// for details about element visibility.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::proxy;
    ///
    /// // Create a private proxy to stdout.
    /// let target = std::io::stdout();
    /// let proxy = proxy::Builder::new()
    ///               .private()
    ///               .create(target);
    ///
    /// assert!(proxy.is_ok());
    /// ```
    pub fn private(mut self) -> Self {
        self.visible = false;
        self
    }

    /// Enable the output direction only.
    ///
    /// Data may only be written through the proxy, any attempt to
    /// read through the latter would yield an error.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::proxy;
    ///
    /// // Create a proxy to stdout.
    /// let target = std::io::stdout();
    /// let proxy = proxy::Builder::new()
    ///               .output_only()
    ///               .create(target);
    ///
    /// assert!(proxy.is_ok());
    /// ```
    pub fn output_only(mut self) -> Self {
        self.output = true;
        self.input = false;
        self
    }

    /// Enable the input direction only.
    ///
    /// Data may only be read through the proxy, any attempt to write
    /// through the latter would yield an error.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::proxy;
    ///
    /// // Create a proxy to stdout.
    /// let target = std::io::stdout();
    /// let proxy = proxy::Builder::new()
    ///               .input_only()
    ///               .create(target);
    ///
    /// assert!(proxy.is_ok());
    /// ```
    pub fn input_only(mut self) -> Self {
        self.output = false;
        self.input = true;
        self
    }

    /// Enable the both input and output directions.
    ///
    /// Data may either be read or written through the proxy.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::proxy;
    ///
    /// // Create a proxy to stdout.
    /// let target = std::io::stdout();
    /// let proxy = proxy::Builder::new()
    ///               .input_output()
    ///               .create(target);
    ///
    /// assert!(proxy.is_ok());
    /// ```
    pub fn input_output(mut self) -> Self {
        self.output = true;
        self.input = true;
        self
    }

    /// Set the I/O buffer size.
    ///
    /// Zero is an acceptable value if you plan to use this proxy
    /// exclusively for exporting a shared memory mapping, in which
    /// case there is no point in reserving any I/O buffer space.  In
    /// absence of I/O buffer space, data may be neither be read
    /// through nor written through the proxy, any attempt to do so
    /// would yield an error.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::proxy;
    ///
    /// // Create a proxy with 16k buffer to stdout.
    /// let target = std::io::stdout();
    /// let proxy = proxy::Builder::new()
    ///               .buf_size(16384)
    ///               .create(target);
    ///
    /// assert!(proxy.is_ok());
    /// ```
    pub fn buf_size(mut self, size: usize) -> Self {
        self.bufsz = size;
        self
    }

    /// Set the I/O granularity.
    ///
    /// In some cases, the target file may have special semantics,
    /// which requires a fixed amount of data to be submitted at each
    /// read/write operation, like the [`eventfd(2)`] interface which
    /// requires 64-bit words to be read or written from/to it at each
    /// transfer.
    ///
    /// When granularity is less than 2, the proxy is free to read or
    /// write as many bytes as possible from/to the target file at
    /// each transfer performed by the worker. Conversely, a
    /// granularity value greater than 1 is used as the exact count of
    /// bytes which may be read from or written to the target file by
    /// the in-band worker at each transfer. You may pass zero for a
    /// memory mapping proxy since no granularity is applicable in
    /// this case.
    ///
    /// # Examples
    ///
    /// ```ignore
    /// use eventfd;
    /// use revl::proxy;
    ///
    /// // Create a proxy to an eventfd with a 4-byte output granularity.
    /// let target = EventFD::new(0, EfdFlags::empty());
    /// let proxy = proxy::Builder::new()
    ///               .granularity(std::mem::size_of::<u64>())
    ///               .create(target);
    ///
    /// assert!(proxy.is_ok());
    /// ```
    ///
    /// [`eventfd(2)`]: http://man7.org/linux/man-pages/man2/eventfd.2.html
    pub fn granularity(mut self, granularity: usize) -> Self {
        self.granularity = granularity;
        self
    }

    /// Turn on non-blocking mode.
    ///
    /// Use this property to prevent threads from blocking during I/O
    /// operations as a result of a buffer underflow/overflow,
    /// receiving the `ErrorKind::WouldBlock` error status instead.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::proxy;
    ///
    /// // Create a non-blocking proxy to stdout.
    /// let target = std::io::stdout();
    /// let proxy = proxy::Builder::new()
    ///               .nonblock()
    ///               .create(target);
    ///
    /// assert!(proxy.is_ok());
    /// ```
    pub fn nonblock(mut self) -> Self {
        self.nonblock = true;
        self
    }

    /// Create a new proxy out of this builder.
    ///
    /// `target` is the proxied data source and/or sink, which must
    /// implement the AsRawFd trait.
    ///
    /// # Errors
    ///
    /// Any error from [Proxy::new()](Proxy::new) may be returned.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::proxy;
    ///
    /// // Create a publicly visible proxy to stdout.
    /// let target = std::io::stdout();
    /// let proxy = proxy::Builder::new()
    ///               .name(&format!("some_proxy-{}", std::process::id()))
    ///               .public()
    ///               .create(target);
    ///
    /// assert!(proxy.is_ok());
    /// ```
    pub fn create(self, target: impl AsRawFd) -> Result<Proxy, Error> {
        Proxy::new(target, self)
    }
}

#[derive(Default, PartialEq, Debug)]
pub struct Proxy(pub(crate) c_int);

unsafe impl Send for Proxy {}
unsafe impl Sync for Proxy {}

impl Proxy {
    /// Create an EVL proxy, retrieving the settings from a [`builder
    /// struct`](Builder).
    ///
    /// The [`Builder`] struct contains the EVL-specific properties to
    /// use. `target` is the proxied data source and/or sink, which
    /// must implement the AsRawFd trait.
    ///
    /// # Errors
    ///
    /// [`ErrorKind::AlreadyExists`] if the generated name is
    /// conflicting with an existing proxy name.
    ///
    /// [`ErrorKind::InvalidInput`] if the generated name is badly
    /// formed, or the overall length of the device element's file
    /// path including the generated name exceeds PATH_MAX.
    ///
    /// [`ErrorKind::OutOfMemory`] if not enough memory was available
    /// for completing the operation. By extension, this error status
    /// is also returned if the per-process limit on the number of
    /// open file descriptors or the system-wide limit on the total
    /// number of open files has been reached.
    ///
    /// [`ErrorKind::Other`] on unexpected error.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::proxy::{self, Proxy};
    ///
    /// // Create a proxy to stdout.
    /// let target = std::io::stdout();
    /// let builder = proxy::Builder::new()
    ///               .name(&format!("some_proxy-{}", std::process::id()))
    ///               .public();
    ///
    /// assert!(Proxy::new(target, builder).is_ok());
    /// ```
    pub fn new(target: impl AsRawFd, builder: Builder) -> Result<Self, Error> {
        let c_targetfd: c_int = target.as_raw_fd().into();
	let mut c_flags = CloneFlags::PRIVATE.bits() as c_int;
        if builder.visible {
	    c_flags = CloneFlags::PUBLIC.bits() as c_int;
        }
        let c_bufsz = builder.bufsz;
        let c_granularity = builder.granularity;
        // The core wants us to omit the I/O direction if bufsz is
        // zero.
        if c_bufsz > 0 {
            if builder.output {
                c_flags |= CloneFlags::OUTPUT.bits() as c_int;
            }
            if builder.input {
                c_flags |= CloneFlags::INPUT.bits() as c_int;
            }
        }
        if builder.nonblock {
            c_flags |= CloneFlags::NONBLOCK.bits() as c_int;
        }
	let pfd: c_int = unsafe {
            if let Some(name) = builder.name {
	        let c_name = CString::new(name).map_err(
                    |_| Error::from(ErrorKind::OutOfMemory)
                )?;
	        let c_fmt = CString::new("%s").map_err(
                    |_| Error::from(ErrorKind::OutOfMemory)
                )?;
	        evl_create_proxy(
                    c_targetfd, c_bufsz, c_granularity, c_flags, c_fmt.as_ptr(), c_name.as_ptr()
                )
            } else {
	        evl_create_proxy(c_targetfd, c_bufsz, c_granularity, c_flags, ptr::null())
            }
	};
	// evl_create_proxy() returns a valid file descriptor or -errno.
	match pfd {
            0.. => {
                Ok(Proxy(pfd))
            },
            e if -e == libc::EEXIST => {
                Err(Error::from(ErrorKind::AlreadyExists))
            },
            e if -e == libc::EINVAL || -e == libc::ENAMETOOLONG => {
                Err(Error::from(ErrorKind::InvalidInput))
            },
            e if -e == libc::ENOMEM || -e == libc::EMFILE || -e == libc::ENFILE => {
                Err(Error::from(ErrorKind::OutOfMemory))
            },
            _ => {
                Err(Error::from(ErrorKind::Other))
            },
	}
    }
}

impl Drop for Proxy {
    fn drop(&mut self) {
        unsafe {
            libc::close(self.0);
        }
    }
}

impl Write for Proxy {
    /// Write to the real-time end of a proxy.
    ///
    /// Channels the content of `buf` through the proxy, without
    /// incurring any transition to the in-band stage. The count of
    /// bytes written through the proxy is returned on success.
    ///
    /// # Errors
    ///
    /// [`ErrorKind::InvalidInput`] if either the slice is larger than
    /// the size of the [`output buffer`](Builder::buf_size), or the
    /// number of bytes in the slice is not a multiple of the
    /// [`output granularity`](Builder::granularity).
    ///
    /// [`ErrorKind::Unsupported`] if the proxy is not available for
    /// output.
    ///
    /// [`ErrorKind::Other`] on unexpected error.
    ///
    fn write(&mut self, buf: &[u8]) -> Result<usize, Error> {
        let data = buf.as_ptr() as *const c_void;
        let ret = unsafe { evl_write_proxy(self.0, data, buf.len()) };
        match ret {
            count if count >= 0 => {
                Ok(count as usize)
            },
            e if -e == libc::EINVAL as isize => {
                Err(Error::from(ErrorKind::InvalidInput))
            },
            e if -e == libc::EFBIG as isize => {
                Err(Error::from(ErrorKind::InvalidInput))
            },
            e if -e == libc::ENXIO as isize => {
                Err(Error::from(ErrorKind::Unsupported))
            },
            _ => {
                Err(Error::from(ErrorKind::Other))
            }
        }
    }

    /// Flush the output buffered on the real-time end of a proxy.
    ///
    /// This call has no effect since the implementation uses
    /// unbuffered I/O in user-space.
    fn flush(&mut self) -> Result<(), Error> {
        Ok(())
    }
}

impl Read for Proxy {
    /// Read from the real-time end of a proxy.
    ///
    /// Fills the content of `buf` with input read from the proxy,
    /// without incurring any transition to the in-band stage. The
    /// count of bytes read through the proxy is returned on success.
    ///
    /// # Errors
    ///
    /// [`ErrorKind::InvalidInput`] if either the slice is larger than
    /// the size of the [`input buffer`](Builder::buf_size), or the
    /// number of bytes in the slice is not a multiple of the
    /// [`input granularity`](Builder::granularity).
    ///
    /// [`ErrorKind::Unsupported`] if the proxy is not available for
    /// input.
    ///
    /// [`ErrorKind::PermissionDenied`] if the calling thread is not
    /// attached to the EVL core.
    ///
    /// [`ErrorKind::Other`] on unexpected error.
    ///
    fn read(&mut self, buf: &mut [u8]) -> Result<usize, Error> {
        let data = buf.as_mut_ptr() as *mut c_void;
        let ret = unsafe { evl_read_proxy(self.0, data, buf.len()) };
        match ret {
            count if count >= 0 => {
                Ok(count as usize)
            },
            e if -e == libc::EINVAL as isize => {
                Err(Error::from(ErrorKind::InvalidInput))
            },
            e if -e == libc::EFBIG as isize => {
                Err(Error::from(ErrorKind::InvalidInput))
            },
            e if -e == libc::ENXIO as isize => {
                Err(Error::from(ErrorKind::Unsupported))
            },
            e if -e == libc::EPERM as isize => {
                Err(Error::from(ErrorKind::PermissionDenied))
            },
            _ => {
                Err(Error::from(ErrorKind::Other))
            }
        }
    }
}

// Make sure to bootstrap the proxies to the standard stream if need
// be. Although such bootstrap would require in-band services, the
// fact that it is still necessary at this point means that no thread
// ever attached to the core yet, therefore there is no way we could
// downgrade from the oob stage unexpectedly because the caller cannot
// be an EVL thread in the first place.
#[inline(always)]
fn init_std_streams() {
    STD_INIT.call_once(|| { let _ = crate::init(); });
}

pub fn inner_print(args: fmt::Arguments) -> Result<(), Error> {
    init_std_streams();
    // We know evl_stdout() is initialized, either with -EBADF at load
    // up, or with a valid fd if init_std_streams() ever succeeded
    // next. Either way, libevl returns sane information.
    Proxy(unsafe { evl_sys::evl_stdout() }).write_fmt(args)
}

/// Prints to the standard output stream via a proxy.
///
/// EVL equivalent to the [`print!`] macro except the output goes
/// through a proxy connected to the standard output stream of the
/// calling process. As a result, using this macro will **NOT** cause
/// the caller to be downgraded to the in-band stage.
#[macro_export]
macro_rules! evl_print {
    ($($arg:tt)*) => {{
        $crate::proxy::inner_print(std::format_args!($($arg)*));
    }};
}

/// Prints to the standard output stream via a proxy.
///
/// Equivalent to the [`evl_print!`] macro except an ending newline
/// character is added to the output.
#[macro_export]
macro_rules! evl_println {
    () => {
        $crate::evl_print!("\n")
    };
    ($($arg:tt)*) => {{
        $crate::proxy::inner_print(std::format_args!($($arg)*));
        $crate::evl_print!("\n");  // Until we have format_args_nl() in stable.
    }};
}

pub fn inner_eprint(args: fmt::Arguments) -> Result<(), Error> {
    init_std_streams();
    // Same logic as inner_print() wrt evl_stderr() sanity.
    Proxy(unsafe { evl_sys::evl_stderr() }).write_fmt(args)
}

/// Prints to the standard error stream via a proxy.
///
/// EVL equivalent to the [`eprint!`] macro except the output goes
/// through a proxy connected to the standard error stream of the
/// calling process. As a result, using this macro will **NOT** cause
/// the caller to be downgraded to the in-band stage.
#[macro_export]
macro_rules! evl_eprint {
    ($($arg:tt)*) => {{
        $crate::proxy::inner_eprint(std::format_args!($($arg)*));
    }};
}

/// Prints to the standard error stream via a proxy.
///
/// Equivalent to the [`evl_eprint!`] macro except an ending newline
/// character is added to the output.
#[macro_export]
macro_rules! evl_eprintln {
    () => {
        $crate::evl_eprint!("\n")
    };
    ($($arg:tt)*) => {{
        $crate::proxy::inner_eprint(std::format_args!($($arg)*));
        $crate::evl_eprint!("\n");  // Until we have format_args_nl() in stable.
    }};
}
