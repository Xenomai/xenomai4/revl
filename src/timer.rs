//! Interface to EVL timers
//!
//! Applications may likely need to run timed loops waiting for
//! high-precision clock events, either once or according to a
//! recurring period: the Timer struct provides such support based on
//! a selectable clock.
//!
//! Timers support synchronous delivery only, there is no asynchronous
//! notification mechanism of clock events. This means that a client
//! thread must explicitly wait for the next expiry.
//!
//! The Timer struct is the Rust interface to EVL's [timer
//! feature](https://evlproject.org/core/user-api/timer/).

use std::ptr;
use std::io::{
    Error,
    ErrorKind
};
use std::mem::MaybeUninit;
use libc::{
    self,
    c_int,
    c_long,
    c_void,
    time_t,
    close,
};
use evl_sys::{
    evl_new_timer,
    evl_set_timer,
    evl_get_timer,
    oob_read,
    timespec,
    itimerspec,
};
use embedded_time::{
    duration::{Nanoseconds, Seconds},
    rate::*,
    Instant,
};
use crate::clock::CoreClock;

/// A timer based on a particular clock.
#[derive(PartialEq, Debug)]
pub struct Timer(pub(crate) c_int);

impl Timer {
    /// Create a new timer.
    ///
    /// Time is measured from readouts of a particular `clock`,
    /// typically the [`STEADY_CLOCK`](crate::clock::STEADY_CLOCK) or
    /// the [`SYSTEM_CLOCK`](crate::clock::SYSTEM_CLOCK).
    ///
    /// # Errors
    ///
    /// [`ErrorKind::OutOfMemory`] if not enough memory was available
    /// for completing the operation. By extension, this error status
    /// is also returned if the per-process limit on the number of
    /// open file descriptors or the system-wide limit on the total
    /// number of open files has been reached.
    ///
    /// [`ErrorKind::Other`] on unexpected error.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use revl::clock::STEADY_CLOCK;
    /// use revl::timer::*;
    ///
    /// // Create a timer based on the monotonic clock.
    /// assert!(Timer::new(STEADY_CLOCK).is_ok());
    /// ```
    pub fn new(clock: CoreClock) -> Result<Self, Error> {
        let tmfd: c_int = unsafe {
            evl_new_timer(clock.0 as c_int)
        };
        match tmfd {
            0.. => {
                Ok(Timer(tmfd))
            },
            e if -e == libc::ENOMEM || -e == libc::EMFILE || -e == libc::ENFILE => {
                Err(Error::from(ErrorKind::OutOfMemory))
            },
            _ => {
                Err(Error::from(ErrorKind::Other))
            }
        }
    }

    /// Arm a oneshot timer.
    ///
    /// Set the next timeout date of a oneshot timer.
    ///
    /// # Errors
    ///
    /// [`ErrorKind::InvalidInput`] if the absolute expiry `date` is
    /// wrong. A date in the past is acceptable, although the timer is
    /// unlikely to fire anytime soon.
    ///
    /// [`ErrorKind::Other`] on unexpected error.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use embedded_time::{
    ///     duration::*,
    ///     Clock,
    ///     Instant,
    /// };
    /// use revl::clock::STEADY_CLOCK;
    /// use revl::timer::*;
    ///
    /// // Create a timer based on the monotonic clock.
    /// let timer = Timer::new(STEADY_CLOCK).unwrap();
    /// // Arm a timer to fire once in one sec from now.
    /// let date = STEADY_CLOCK.now() + Seconds(1_u32);
    /// assert!(timer.arm(date).is_ok());
    /// ```
    pub fn arm(&self, date: Instant<CoreClock>) -> Result<(), Error> {
        let dur = date.duration_since_epoch();
        let secs: Seconds<u64> = Seconds::try_from(dur).map_err(
            |_| Error::from(ErrorKind::InvalidInput)
        )?;
        let nsecs: Nanoseconds<u64> = Nanoseconds::<u64>::try_from(dur).map_err(
            |_| Error::from(ErrorKind::InvalidInput)
        )? % secs;
        let it = itimerspec {
            it_value: timespec {
                tv_sec: secs.integer() as time_t,
                tv_nsec: nsecs.integer() as c_long,
            },
            it_interval: timespec {
                tv_sec: 0,
                tv_nsec: 0,
            }
        };
        let ret: c_int = unsafe { evl_set_timer(self.0 as c_int, &it, ptr::null_mut()) };
        match -ret {
            0 => {
                Ok(())
            },
            libc::EINVAL => {
                Err(Error::from(ErrorKind::InvalidInput))
            },
            _ => {
                Err(Error::from(ErrorKind::Other))
            },
        }
    }

    /// Arm a periodic timer.
    ///
    /// Set the next timeout date and period of a periodic timer.
    ///
    /// # Errors
    ///
    /// [`ErrorKind::InvalidInput`] if any of the first expiry `date`
    /// or `period` value is wrong. A date in the past is acceptable,
    /// although the timer is unlikely to fire anytime soon.
    ///
    /// [`ErrorKind::Other`] on unexpected error.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use embedded_time::{
    ///     duration::*,
    ///     Clock,
    ///     Instant,
    /// };
    /// use revl::clock::STEADY_CLOCK;
    /// use revl::timer::*;
    ///
    /// // Create a timer based on the monotonic clock.
    /// let timer = Timer::new(STEADY_CLOCK).unwrap();
    /// // Arm a timer to fire every 100µs, starting in one sec from now.
    /// let date = STEADY_CLOCK.now() + Seconds(1_u32);
    /// let period = Microseconds(100_u32);
    /// assert!(timer.arm_periodic(date, period.into()).is_ok());
    /// ```
    pub fn arm_periodic(&self, date: Instant<CoreClock>, period: Nanoseconds<u64>) -> Result<(), Error> {
        let dur = date.duration_since_epoch();
        let v_secs: Seconds<u64> = Seconds::try_from(dur).map_err(
            |_| Error::from(ErrorKind::InvalidInput)
        )?;
        let v_nsecs: Nanoseconds<u64> = Nanoseconds::<u64>::try_from(dur).map_err(
            |_| Error::from(ErrorKind::InvalidInput)
        )? % v_secs;
        let p_secs: Seconds<u64> = Seconds::try_from(period).map_err(
            |_| Error::from(ErrorKind::InvalidInput)
        )?;
        let p_nsecs: Nanoseconds<u64> = Nanoseconds::<u64>::try_from(period).map_err(
            |_| Error::from(ErrorKind::InvalidInput)
        )? % p_secs;
        let it = itimerspec {
            it_value: timespec {
                tv_sec: v_secs.integer() as time_t,
                tv_nsec: v_nsecs.integer() as c_long,
            },
            it_interval: timespec {
                tv_sec: p_secs.integer() as time_t,
                tv_nsec: p_nsecs.integer() as c_long,
            }
        };
        let ret: c_int = unsafe { evl_set_timer(self.0 as c_int, &it, ptr::null_mut()) };
        match -ret {
            0 => {
                Ok(())
            },
            libc::EINVAL => {
                Err(Error::from(ErrorKind::InvalidInput))
            },
            _ => {
                Err(Error::from(ErrorKind::Other))
            },
        }
    }

    /// Disarm a timer.
    ///
    /// Stop the timer from running, preventing any further
    /// tick. Requests to disarm a stopped timer are silently ignored.
    ///
    /// [`ErrorKind::Other`] on unexpected error.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use embedded_time::{
    ///     duration::*,
    ///     Clock,
    ///     Instant,
    /// };
    /// use revl::clock::STEADY_CLOCK;
    /// use revl::timer::*;
    ///
    /// // Create a timer based on the monotonic clock.
    /// let timer = Timer::new(STEADY_CLOCK).unwrap();
    /// // Arm a timer to fire once in one sec from now.
    /// let date = STEADY_CLOCK.now() + Seconds(1_u32);
    /// assert!(timer.arm(date).is_ok());
    /// // Then disarm it.
    /// assert!(timer.disarm().is_ok());
    /// ```
    pub fn disarm(&self) -> Result<(), Error> {
        let it = itimerspec {
            it_value: timespec {
                tv_sec: 0,
                tv_nsec: 0,
            },
            it_interval: timespec {
                tv_sec: 0,
                tv_nsec: 0,
            }
        };
        let ret: c_int = unsafe { evl_set_timer(self.0 as c_int, &it, ptr::null_mut()) };
        match ret {
            0 => {
                Ok(())
            },
            _ => {
                Err(Error::from(ErrorKind::Other))
            },
        }
    }

    /// Get a timer settings.
    ///
    /// Retrieve the current settings of a timer, i.e. its expiry date
    /// and optional period. Returns None if disarmed on entry.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use embedded_time::{
    ///     duration::*,
    ///     Clock,
    ///     Instant,
    /// };
    /// use revl::clock::STEADY_CLOCK;
    /// use revl::timer::*;
    ///
    /// // Create a timer based on the monotonic clock.
    /// let timer = Timer::new(STEADY_CLOCK).unwrap();
    /// // Arm a timer to fire every 500µs, starting in one sec from now.
    /// let date = STEADY_CLOCK.now() + Seconds(1_u32);
    /// let period: Nanoseconds<u64> = Microseconds(500_u32).into();
    /// assert!(timer.arm_periodic(date, period).is_ok());
    /// // Fetch the timer settings.
    /// let (expiry, interval) = timer.get().unwrap();
    /// assert_eq!(expiry, date); 
    /// assert_eq!(period, interval); 
    /// ```
    pub fn get(&self) -> Option<(Instant<CoreClock>, Nanoseconds<u64>)> {
	let mut its = MaybeUninit::<itimerspec>::uninit();
        let ret: c_int = unsafe { evl_get_timer(self.0 as c_int, its.as_mut_ptr()) };
        fn ts_to_ns(ts: &timespec) -> u64 {
            ts.tv_sec as u64 * 1_000_000_000 + ts.tv_nsec as u64
        }
        match ret {
            0 => {
                let its = unsafe { its.assume_init() };
                let expiry_ns = ts_to_ns(&its.it_value);
                let interval_ns = ts_to_ns(&its.it_interval);
                (expiry_ns != 0 || interval_ns != 0).then(
                    || (Instant::new(expiry_ns), Nanoseconds::<u64>(interval_ns))
                )
            },
            _ => {
                // Uuh? Should never get there, but being panicking
                // via unreachable!() is not acceptable either.
                None
            },
        }
    }

    /// Wait for the next timer event.
    ///
    /// Sleep until the next timer event is received. Returns an
    /// optional count of overruns on success. Late wakeups are
    /// denoted by receiving Some(count) where count is the number of
    /// missed periods, aka overruns. Conversely, receiving Ok(None)
    /// denotes on-time wakeup after a successful wait.
    ///
    /// # Errors
    ///
    /// [`ErrorKind::PermissionDenied`] if the calling thread is not
    /// attached to the EVL core.
    ///
    /// [`ErrorKind::Other`] on unexpected error.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use embedded_time::{
    ///     duration::*,
    ///     Clock,
    ///     Instant,
    /// };
    /// use revl::clock::STEADY_CLOCK;
    /// use revl::timer::*;
    /// use revl::evl_println;
    ///
    /// // Create a timer based on the monotonic clock.
    /// let timer = Timer::new(STEADY_CLOCK).unwrap();
    /// // Arm a timer to fire every 500µs, starting in one sec from now.
    /// let date = STEADY_CLOCK.now() + Seconds(1_u32);
    /// let period: Nanoseconds<u64> = Microseconds(500_u32).into();
    /// assert!(timer.arm_periodic(date, period).is_ok());
    ///
    /// // Loop waiting for events which should occur at 2Hz.
    /// loop {
    ///     if let Some(overruns) = timer.wait().unwrap() {
    ///         evl_println!("late from {} ticks?!", overruns);
    ///     } else {
    ///         // All good, we are on time to process the current period.
    ///     }
    /// }
    /// ```
    pub fn wait(&self) -> Result<Option<u64>, Error> {
	let mut overruns = MaybeUninit::<u64>::uninit();
        let ret = unsafe {
            oob_read(self.0 as c_int,
                     overruns.as_mut_ptr() as *mut c_void,
                     std::mem::size_of::<u64>())
        } as i32;
        match -ret {
            0 => {
                let overruns = unsafe { overruns.assume_init() };
                Ok((overruns > 0).then(|| overruns))
            },
            libc::EPERM => {
                Err(Error::from(ErrorKind::PermissionDenied))
            },
            _ => {
                Err(Error::from(ErrorKind::Other))
            },
        }
    }
}

impl Drop for Timer {
    fn drop(&mut self) {
        unsafe {
            close(self.0);
        }
    }
}
